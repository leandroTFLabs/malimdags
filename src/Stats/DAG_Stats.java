/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Stats;

import Auxiliar.Clustering.Path_Label;
import Forest.DAG;
import Forest.Path;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author ASUS
 */
public class DAG_Stats {
    /*
    ATRIBUTS
    */
    private DAG dag_corresp;
    private StatsMaker stats_maker_L;
    private StatsMaker stats_maker_CL;
    private StatsMaker stats_maker_labels;
    private StatsMaker stats_maker_labels_ns;
    ///////////////////////////////////////////////
    /*
    CONSTRUCTORS
    */
    public DAG_Stats(DAG dag_corresp) {
        this.stats_maker_L = new StatsMaker();
        this.stats_maker_CL = new StatsMaker();
        this.stats_maker_labels = new StatsMaker();
        this.stats_maker_labels_ns = new StatsMaker();
        this.dag_corresp = dag_corresp;
    }
    ///////////////////////////////////////////////
    /*
    GETTERS
    */
    ///////////////////////////////////////////////
    /*
    SETTERS
    */
    public void calculaStats() {
        for (Path p : dag_corresp.getDagPaths()) {
            stats_maker_L.afegeixValor((double) p.getPathSize());
        }
        stats_maker_L.calculaStats();
        HashMap<Double,Integer> aparicions_valors = new HashMap();
        HashMap<Path_Label,Integer> aparicions_grups = new HashMap();
        HashMap<Path_Label,Integer> aparicions_grups_sense_sigma = new HashMap();
        for (Path p : dag_corresp.getDagPaths()) {
            double val = (double) p.getPathSize();
            if (aparicions_valors.containsKey(val)) {
                aparicions_valors.put(val, aparicions_valors.get(val) + 1);
            } else {
                aparicions_valors.put(val, 1);
            }
            
            Path_Label pl = p.getEtiqCami();
            Path_Label plns = new Path_Label(pl.getPtwCnt(),pl.getSigCnt(), new ArrayList());
            plns.setBearSigm(false);
            if (aparicions_grups.containsKey(pl)) {
                aparicions_grups.put(pl, aparicions_grups.get(pl) + 1);
            } else {
                aparicions_grups.put(pl, 1);
            }
            if (aparicions_grups_sense_sigma.containsKey(plns)) {
                aparicions_grups_sense_sigma.put(plns, aparicions_grups_sense_sigma.get(plns) + 1);
            } else {
                aparicions_grups_sense_sigma.put(plns, 1);
            }
        }
        for (double clau : aparicions_valors.keySet()) {
            stats_maker_CL.afegeixValor(aparicions_valors.get(clau));
        }
        for (Path_Label pl : aparicions_grups.keySet()) {
            stats_maker_labels.afegeixValor(aparicions_grups.get(pl));
        }
        for (Path_Label plns : aparicions_grups_sense_sigma.keySet()) {
            stats_maker_labels_ns.afegeixValor(aparicions_grups_sense_sigma.get(plns));
        }
        stats_maker_CL.calculaStats();
        stats_maker_labels.calculaStats();
        stats_maker_labels_ns.calculaStats();
    }
    ///////////////////////////////////////////////
    /*
    ALTRES MÈTODES AUXILIARS
    */
    ///////////////////////////////////////////////
    @Override
    public String toString() {
        //linies=17 //LONGITUD DELS CAMINS
        String desc_L = "ESTADÍSTIQUES REFERENTS A LA LONGITUD DELS CAMINS\r\n";
        String n_valors_L = "\tN longituds distintes = " + stats_maker_L.getNValorsDistints() + "\r\n";
        String mitjana_L = "\tMitjana de longitud dels camins = " + stats_maker_L.getMitjana() + "\r\n";
        String variança_L = "\tVariança en la longitud dels camins = " + stats_maker_L.getVariança() + "\r\n";
        String desviació_L = "\tDesviació típica en la longitud dels camins = " + stats_maker_L.getDesviació() + "\r\n";
        String desc_CL = "ESTADÍSTIQUES REFERENTS AL NOMBRE D'APARICIONS DE CADA LONGITUD CONCRETA\r\n";
        String n_valors_CL = "\tN popularitats distintes per a cada longitud = " + stats_maker_CL.getNValorsDistints() + "\r\n";
        String valor_minim_CL = "\tValor mínim en les aparicions per longitud = " + stats_maker_CL.getValorMinim() + "\r\n";
        String valor_maxim_CL = "\tValor màxim en les aparicions per longitud = " + stats_maker_CL.getValorMaxim() + "\r\n";
        String mitjana_CL = "\tMitjana de popularitat per longitud = " + stats_maker_CL.getMitjana() + "\r\n";
        String variança_CL = "\tVariança en la popularitat per longitud = " + stats_maker_CL.getVariança() + "\r\n";
        String desviació_CL = "\tDesviació típica en la popularitat per longitud = " + stats_maker_CL.getDesviació() + "\r\n";
        String desc_grups = "ESTADÍSTIQUES REFERENTS ALS GRUPS (ETIQUETES) DELS CAMINS\r\n";
        String n_valors_grups = "\tN tamanys de grup distints = " + stats_maker_labels.getNValorsDistints() + "\r\n";
        String valor_minim_grups = "\tValor mínim en el tamany de grups = " + stats_maker_labels.getValorMinim() + "\r\n";
        String valor_maxim_grups = "\tValor màxim en el tamany de grups = " + stats_maker_labels.getValorMaxim() + "\r\n";
        String mitjana_grups = "\tMitjana de tamany de grup = " + stats_maker_labels.getMitjana() + "\r\n";
        String variança_grups = "\tVariança en el tamany dels grups = " + stats_maker_labels.getVariança() + "\r\n";
        String desviació_grups = "\tDesviació típica en el tamany dels grups = " + stats_maker_labels.getDesviació() + "\r\n";
        String desc_grups_ns = "ESTADÍSTIQUES REFERENTS ALS GRUPS PRIMARIS (ETIQUETES SENSE COMPTAR SUBETIQUETES - SIGMES PER GRUP DE PATHWAYS) DELS CAMINS\r\n";
        String n_valors_grups_ns = "\tN tamanys de grup primari distints = " + stats_maker_labels_ns.getNValorsDistints() + "\r\n";
        String valor_minim_grups_ns = "\tValor mínim en el tamany de grups primaris = " + stats_maker_labels_ns.getValorMinim() + "\r\n";
        String valor_maxim_grups_ns = "\tValor màxim en el tamany de grups primaris = " + stats_maker_labels_ns.getValorMaxim() + "\r\n";
        String mitjana_grups_ns = "\tMitjana de tamany de grup primari = " + stats_maker_labels_ns.getMitjana() + "\r\n";
        String variança_grups_ns = "\tVariança en el tamany dels grups primaris = " + stats_maker_labels_ns.getVariança() + "\r\n";
        String desviació_grups_ns = "\tDesviació típica en el tamany dels grups primaris = " + stats_maker_labels_ns.getDesviació() + "\r\n";
        return
                desc_L +
                n_valors_L +
                mitjana_L +
                variança_L +
                desviació_L +
                desc_CL +
                n_valors_CL +
                valor_minim_CL +
                valor_maxim_CL +
                mitjana_CL +
                variança_CL +
                desviació_CL +
                desc_grups +
                n_valors_grups +
                valor_minim_grups +
                valor_maxim_grups +
                mitjana_grups +
                variança_grups +
                desviació_grups +
                desc_grups_ns +
                n_valors_grups_ns +
                valor_minim_grups_ns +
                valor_maxim_grups_ns +
                mitjana_grups_ns +
                variança_grups_ns +
                desviació_grups_ns;
    }
}

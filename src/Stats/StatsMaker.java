/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Stats;

import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class StatsMaker {
    /*
    ATRIBUTS
    */
    private ArrayList<Double> llista_valors;
    private ArrayList<Double> valors_unics;
    private int n_valors_distints;
    private double valor_minim = Double.MAX_VALUE;
    private double valor_maxim = Double.MIN_VALUE;
    private double mitjana;
    private double variança;
    private double desviació;
    ///////////////////////////////////////////////
    /*
    CONSTRUCTORS
    */
    public StatsMaker(ArrayList<Double> llista_valors) {
        this.llista_valors = llista_valors;
        this.valors_unics = new ArrayList();
        calculaStats();
    }
    public StatsMaker() {
        this.llista_valors = new ArrayList();
        this.valors_unics = new ArrayList();
    }
    ///////////////////////////////////////////////
    /*
    GETTERS
    */
    public double getNValorsDistints() {
        return n_valors_distints;
    }
    public double getValorMinim() {
        return valor_minim;
    }
    public double getValorMaxim() {
        return valor_maxim;
    }
    public double getMitjana() {
        return mitjana;
    }
    public double getVariança() {
        return variança;
    }
    public double getDesviació() {
        return desviació;
    }
    ///////////////////////////////////////////////
    /*
    SETTERS
    */
    public void afegeixValor(double valor) {
        this.llista_valors.add(valor);
    }
    ///////////////////////////////////////////////
    /*
    ALTRES MÈTODES AUXILIARS
    */
    ///////////////////////////////////////////////
    public void calculaStats() {
        //n valors distints
        double suma_total = 0;
        for (double valor : llista_valors) {
            suma_total += valor;
            if (!valors_unics.contains(valor)) {
                if (valor < valor_minim) valor_minim = valor;
                if (valor > valor_maxim) valor_maxim = valor;
                valors_unics.add(valor);
            }
        }
        this.n_valors_distints = valors_unics.size();
        //mitjana
        this.mitjana = suma_total/llista_valors.size();
        //variança
        double suma_dif_quad = 0;
        for (double valor : llista_valors) {
            suma_dif_quad += Math.pow(this.mitjana-valor,2);
        }
        this.variança = suma_dif_quad/llista_valors.size();
        //desviació típica
        this.desviació = Math.sqrt(this.variança);
    }
}

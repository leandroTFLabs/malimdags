/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auxiliar.Clustering;

import Forest.Node;
import Forest.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

/**
 *
 * @author ASUS
 */
public class Path_Classifier {
    public class Ptw_App {
        private String pathway;
        private double sigma;
        
        public Ptw_App (String pathway, double sigma) {
            this.pathway = pathway;
            this.sigma = sigma;
        }
        
        public String getPathway() {
            return pathway;
        }
        
        public double getSigma() {
            return sigma;
        }
    }
    private class Comparador_Ptw_App implements Comparator<Ptw_App> {
        @Override
        public int compare(Ptw_App o1, Ptw_App o2) {
            return Double.compare(o2.getSigma(), o1.getSigma());
        }
    }
    /*
    ATRIBUTS
    */
    private Path cami_corresp;
    private Path_Class classe_cami;
    ///////////////////////////////////////////////
    /*
    CONSTRUCTORS
    */
    public Path_Classifier(Path cami_corresp) {
        this.cami_corresp = cami_corresp;
        classificaCami();
    }
    ///////////////////////////////////////////////
    /*
    GETTERS
    */
    public Path_Class getClasseCami() {
        return classe_cami;
    }
    ///////////////////////////////////////////////
    /*
    SETTERS
    */
    ///////////////////////////////////////////////
    /*
    ALTRES MÈTODES AUXILIARS
    */
    ///////////////////////////////////////////////
    private void classificaCami() {
        ArrayList<String> classes_conjuntes = new ArrayList();
        for (Node n : this.cami_corresp.getNodesCami()) {
            if (n.getClasse() != null) {
                for (String classe : n.getClasse()) {
                    classes_conjuntes.add(classe);
                }
            }
        }
        if (!classes_conjuntes.isEmpty()) {
            HashMap<String,Integer> aparicions_ptw = new HashMap();
            for (String classe : classes_conjuntes) {
                if (!aparicions_ptw.containsKey(classe)) {
                    aparicions_ptw.put(classe, 1);
                } else {
                    aparicions_ptw.put(classe, aparicions_ptw.get(classe) + 1);
                }
            }
            ArrayList<Ptw_App> pathways_sigmes = new ArrayList();
            int tamany = classes_conjuntes.size();
            for (String classe : aparicions_ptw.keySet()) {
                Ptw_App pa = new Ptw_App(classe,((double) aparicions_ptw.get(classe))/tamany);
                pathways_sigmes.add(pa);
            }
            pathways_sigmes.sort(new Comparador_Ptw_App());
            double sigma_max = pathways_sigmes.get(0).getSigma();
            ArrayList<String> ptw_liders = new ArrayList();
            ArrayList<Double> sig_liders = new ArrayList();
            for (int i = 0; i < pathways_sigmes.size() && pathways_sigmes.get(i).getSigma() == sigma_max; i++) {
                ptw_liders.add(pathways_sigmes.get(i).getPathway());
                sig_liders.add(pathways_sigmes.get(i).getSigma());
            }
            this.classe_cami = new Path_Class(ptw_liders,sig_liders);
        } else {
            ArrayList<String> liders_p = new ArrayList();
            liders_p.add("No_Pathways");
            ArrayList<Double> liders_s = new ArrayList();
            liders_s.add(0.0);
            this.classe_cami = new Path_Class(liders_p,liders_s);
        }
    }
}

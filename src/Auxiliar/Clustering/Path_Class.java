/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auxiliar.Clustering;

import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class Path_Class {
    /*
    ATRIBUTS
    */
    private ArrayList<String> classes_liders;
    private ArrayList<Double> sigmes_liders;
    ///////////////////////////////////////////////
    /*
    CONSTRUCTORS
    */
    public Path_Class(ArrayList<String> classes_liders, ArrayList<Double> sigmes_liders) {
        this.classes_liders = classes_liders;
        this.sigmes_liders = sigmes_liders;
    }
    ///////////////////////////////////////////////
    /*
    GETTERS
    */
    public ArrayList<String> getClassesLiders() {
        return classes_liders;
    }
    ///////////////////////////////////////////////
    /*
    SETTERS
    */
    public ArrayList<Double> getSigmesLiders() {
        return sigmes_liders;
    }
    ///////////////////////////////////////////////
    /*
    ALTRES MÈTODES AUXILIARS
    */
    ///////////////////////////////////////////////
    @Override
    public String toString() {
        String format = "{";
        for (int i = 0; i < classes_liders.size(); i++) {
            format += "[" + classes_liders.get(i)+",";
            format += sigmes_liders.get(i)+"]";
        }
        format += "}";
        return format;
    }
}

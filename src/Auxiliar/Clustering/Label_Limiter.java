/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auxiliar.Clustering;

import Auxiliar.Comparison.Compare_Labels;
import Auxiliar.Clustering.Path_Label;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author ASUS
 */
public class Label_Limiter {
    private HashMap<Path_Label,Integer> label_counter;
    private int sample_size_limit;
    private ArrayList<Path_Label> ordered_labels;
    private boolean labs_ordered;
    public Label_Limiter(int sample_size_limit) {
        this.label_counter = new HashMap();
        this.sample_size_limit = sample_size_limit;
        this.labs_ordered = false;
    }
    public boolean exceedsLimit(Path_Label pl) {
        if (!label_counter.containsKey(pl)) {
            label_counter.put(pl,0);
        }
        return label_counter.get(pl) >= sample_size_limit;
    }
    public void incrementLabel(Path_Label pl) {
        if (!label_counter.containsKey(pl)) {
            label_counter.put(pl,0);
        }
        int cnt = label_counter.get(pl);
        cnt++;
        label_counter.put(pl, cnt);
    }
    public int getLabelCount(Path_Label pl) {
        if (!label_counter.containsKey(pl)) {
            return 0;
        } else {
            return label_counter.get(pl);
        }
    }
    private void orderLabs() {
        ordered_labels = new ArrayList();
        for (Path_Label pl : label_counter.keySet()) {
            ordered_labels.add(pl);
        }
        ordered_labels.sort(new Compare_Labels());
        labs_ordered = true;
    }
    @Override
    public String toString() {
        String to_ret = "";
        if (!labs_ordered) {
            orderLabs();
        }
        HashMap<Path_Label,Integer> prim_groups = new HashMap();
        Path_Label last_prim = null;
        int ind = 0;
        for (Path_Label pl : ordered_labels) {
            int cnt = getLabelCount(pl);
            
            boolean changed = false;
            boolean last = ind == ordered_labels.size()-1;
            if (pl.getBearSigm()) {
                ArrayList<String> ptwl_copy = new ArrayList();
                ptwl_copy.add(pl.getPathway());
                Path_Label prim_copy = new Path_Label(pl.getPtwCnt(),pl.getSigCnt(),ptwl_copy);
                prim_copy.setBearSigm(false);
                if (!prim_groups.containsKey(prim_copy)) {
                    changed = true;
                    prim_groups.put(prim_copy,cnt);
                } else {
                    int last_val = prim_groups.get(prim_copy);
                    prim_groups.put(prim_copy,last_val+cnt);
                }
                if (changed || last) {
                    if (last_prim != null) {
                        String prim_descriptor = "*///PRIMARY SET - "+last_prim.conditionedToString();
                        String prim_value = " =///= APPEARS "+prim_groups.get(last_prim)+" TIMES";
                        to_ret += prim_descriptor+prim_value+"\r\n";
                    }
                    last_prim = new Path_Label(prim_copy.getPtwCnt(),prim_copy.getSigCnt(),ptwl_copy);
                    last_prim.setBearSigm(false);
                }
            }
            
            if (cnt!=0) {
                String descriptor = "* - "+pl.conditionedToString();
                String value = " => appears "+cnt+" times";
                to_ret += descriptor+value+"\r\n";
            }
            
            ind++;
        }
        return to_ret;
    }
}

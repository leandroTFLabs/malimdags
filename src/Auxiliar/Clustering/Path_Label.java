/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auxiliar.Clustering;

import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class Path_Label {

    private int ptw_cnt;
    private int sig_cnt;
    private boolean bear_sigm = true;

    private boolean use_pathw = false;
    private String pathway_nm = "";

    public Path_Label(int ptw_cnt, int sig_cnt, ArrayList<String> pathways) {
        this.ptw_cnt = ptw_cnt;
        this.sig_cnt = sig_cnt;
        if (pathways.size() == 1) {
            this.use_pathw = true;
            this.pathway_nm = pathways.get(0);
        }
    }

    public Path_Label(int ptw_cnt, int sig_cnt, boolean bear_sigm, ArrayList<String> pathways) {
        this.ptw_cnt = ptw_cnt;
        this.sig_cnt = sig_cnt;
        this.bear_sigm = bear_sigm;
        if (pathways.size() == 1) {
            this.use_pathw = true;
            this.pathway_nm = pathways.get(0);
        }
    }

    public int getPtwCnt() {
        return ptw_cnt;
    }

    public int getSigCnt() {
        return sig_cnt;
    }

    public boolean getBearSigm() {
        return bear_sigm;
    }

    public String getPathway() {
        return this.pathway_nm;
    }

    public void setBearSigm(boolean bear_sigm) {
        this.bear_sigm = bear_sigm;
    }

    @Override
    public String toString() {
        return this.use_pathw ? "pathway-"+this.pathway_nm + "_" + sig_cnt : "set-" + ptw_cnt + "_" + sig_cnt;
    }

    public String conditionedToString() {
        return (this.use_pathw ? "pathway-"+this.pathway_nm : "set-" + ptw_cnt) + (bear_sigm ? "_" + sig_cnt : "");
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + this.ptw_cnt;
        if (bear_sigm) {
            hash = 47 * hash + this.sig_cnt;
        }
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Path_Label other = (Path_Label) obj;
        if (this.use_pathw && other.use_pathw && !this.pathway_nm.equals(other.pathway_nm)) {
            return false;
        } else if (this.use_pathw != other.use_pathw) {
            return false;
        } else if (this.ptw_cnt != other.ptw_cnt) {
            return false;
        }
        if (bear_sigm) {
            if (this.sig_cnt != other.sig_cnt) {
                return false;
            }
        }
        return true;
    }

    public static Path_Label getPlFromName(String name, boolean bear_sigm) {
        if (name.startsWith("set-")) {
            String transf = name.replace("set-", "");
            String[] parts = transf.split("_");
            int ptwcnt = Integer.parseInt(parts[0]);
            int sigcnt = bear_sigm ? Integer.parseInt(parts[1]) : -1;
            Path_Label pL = new Path_Label(ptwcnt, sigcnt, bear_sigm, new ArrayList());
            return pL;
        } else {
            name = name.replace("pathway-","");
            String[] parts = name.split("_");
            int ptwcnt = -1;
            int sigcnt = bear_sigm ? Integer.parseInt(parts[1]) : -1;
            String ptname = parts[0];
            ArrayList<String> ptlist = new ArrayList();
            ptlist.add(ptname);
            Path_Label pL = new Path_Label(ptwcnt, sigcnt, bear_sigm, ptlist);
            return pL;
        }
    }
}

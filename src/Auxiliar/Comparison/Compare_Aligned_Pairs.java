/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auxiliar.Comparison;

import Alignments.PathAlignments.Multiple.PITools.Pair_Aligned;
import java.util.Comparator;

/**
 *
 * @author ASUS
 */
public class Compare_Aligned_Pairs implements Comparator<Pair_Aligned> {

    @Override
    public int compare(Pair_Aligned o1, Pair_Aligned o2) {
        return Double.compare(o1.getAlign().getMeanSimil(), o2.getAlign().getMeanSimil());
    }
    
}

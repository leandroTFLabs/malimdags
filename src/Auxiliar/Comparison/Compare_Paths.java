/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auxiliar.Comparison;

import Forest.Path;
import java.util.Comparator;

/**
 *
 * @author ASUS
 */
public class Compare_Paths implements Comparator<Path>{
    @Override
    public int compare(Path p1, Path p2) {
        return p1.getPathSize() > p2.getPathSize() ? -1 : (p1.getPathSize() < p2.getPathSize() ? 1 : (Integer.compare(p1.getIdCami(),p2.getIdCami())));
    }
}

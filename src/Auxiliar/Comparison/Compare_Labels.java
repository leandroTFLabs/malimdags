/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auxiliar.Comparison;

import Auxiliar.Clustering.Path_Label;
import java.util.Comparator;

/**
 *
 * @author ASUS
 */
public class Compare_Labels implements Comparator<Path_Label> {
    @Override
    public int compare(Path_Label pl1, Path_Label pl2) {
        return pl1.getPtwCnt() != pl2.getPtwCnt() || (!pl1.getBearSigm() && !pl2.getBearSigm()) ? Integer.compare(pl1.getPtwCnt(), pl2.getPtwCnt()) : Integer.compare(pl1.getSigCnt(), pl2.getSigCnt());
    }
}

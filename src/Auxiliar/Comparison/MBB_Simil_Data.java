/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auxiliar.Comparison;

import Disk_Mgmt.MBB_Labeler;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader; 
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author ASUS
 */
public class MBB_Simil_Data {
    private final String experiment = "GUT";

    private ArrayList<String> keys;
    private HashMap<String,HashMap<String,Float>> dades_simils;
    
    private MBB_Labeler mbb_labels;
    
    public MBB_Simil_Data() throws FileNotFoundException, IOException {
        String currentDir = System.getProperty("user.dir");
        String similsF = currentDir.concat("/DATA/simil_MBBs/Similarities_MBB_"+experiment+".csv");
        File f = new File(similsF);
        FileReader fr = new FileReader(f);
        BufferedReader br = new BufferedReader(fr);
        
        this.keys = new ArrayList();
        this.dades_simils = new HashMap();
        
        this.mbb_labels = new MBB_Labeler(null);
        
        String first_line = br.readLine();
        while (first_line != null && !first_line.equals("")) {
            boolean delim = first_line.contains(",");
            String this_mbb = delim? first_line.substring(0,first_line.indexOf(",")) : first_line;
            this_mbb = this_mbb.replace(" ", "").replace("\t", "").replace("\"","");
            keys.add(this_mbb);
            first_line = delim ? first_line.substring(first_line.indexOf(",")+1) : "";
        }
        
        String line = br.readLine();
        while (line != null && !line.equals("")) {
            String key = line.substring(0,line.indexOf(","));
            key = key.replace("\"","");
            String line_aux = line.substring(line.indexOf(",")+1);
            int cnt = 0;
            while (line_aux != null && !line_aux.equals("")) {
                boolean delim = line_aux.contains(",");
                String this_val = delim? line_aux.substring(0,line_aux.indexOf(",")) : line_aux;
                this_val = this_val.replace(" ", "").replace("\t", "").replace("\"","");
                float val = Float.parseFloat(this_val);
                HashMap nested = dades_simils.get(key) != null? dades_simils.get(key) : new HashMap();
                String key_n = keys.get(cnt+1);
                nested.put(key_n, val);
                dades_simils.put(key, nested);
                line_aux = delim ? line_aux.substring(line_aux.indexOf(",")+1) : "";
                cnt++;
            }
            
            line = br.readLine();
        }
        fr.close();
    }
    public float getSimilMBBs(String mbbId1, String mbbId2) {
        
        if (mbbId1.equals("-") || mbbId2.equals("-")) return 0.0f;
        
        String id1, id2;
        if (mbbId1.startsWith("MBBL")) {
            //they are labels
            id1 = this.mbb_labels.getMBBBioId(mbbId1);
            id2 = this.mbb_labels.getMBBBioId(mbbId2);
        } else {
            id1 = mbbId1;
            id2 = mbbId2;
        }
        
        if (dades_simils.containsKey(id1)) {
            HashMap nested = dades_simils.get(id1);
            if (nested.containsKey(id2)) {
                return (float) nested.get(id2);
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }
}

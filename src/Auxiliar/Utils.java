/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auxiliar;

import Auxiliar.Clustering.Path_Label;
import Forest.Node;
import Forest.Path;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author ASUS
 */
public class Utils {

    /*
    PATH READING
     */
    public static Path processPathLine(String path_line, boolean bear_sigm) {
        String[] primit_parts = new String[2];
        primit_parts[0] = path_line.substring(0, path_line.indexOf("["));
        primit_parts[1] = path_line.substring(path_line.indexOf("["));
        String path_tokens = primit_parts[1];
        ArrayList<Node> path_nodes = Utils.transformNodesFromString(path_tokens);

        String meta_info = primit_parts[0];
        String[] meta_parts = meta_info.split("/");
        String descriptor = meta_parts[1];
        String label_str = meta_parts[5];

        String dag_name = Utils.transformDAGNameFromPathDescriptor(descriptor);
        int path_id = Utils.transformPathIdFromPathDescriptor(descriptor);
        Path_Label pl = Utils.transformPathLabelFromString(label_str, bear_sigm);

        Path to_ret = new Path(path_id, dag_name, path_nodes);
        to_ret.setEtiqCami(pl);

        return to_ret;
    }

    public static Path findPath(String dag_name, String path_desc, boolean bear_sigm) throws FileNotFoundException, IOException {
        String currentDir = System.getProperty("user.dir");
        String pathwysDir = currentDir.concat("/DATA/calculated/extracted-paths/");
        File fp = new File(pathwysDir.concat(dag_name).concat(".txt"));

        FileReader fr = new FileReader(fp);
        BufferedReader br = new BufferedReader(fr);

        String line = br.readLine();
        Path foundp = null;
        while (line != null && !line.replace("\t", "").replace(" ", "").equals("")) {
            if (line.startsWith("/")) {
                Path p = processPathLine(line, bear_sigm);
                if (p.getDescriptor().equals(path_desc)) {
                    foundp = p;
                    break;
                }
            }

            line = br.readLine();
        }

        return foundp;
    }

    public static ArrayList<Node> transformNodesFromString(String list_string) {
        ArrayList list = new ArrayList();

        list_string = list_string.replace(" ", "").replace("\t", "");
        String[] isolated_node_strings = list_string.split(",");
        for (String node_string : isolated_node_strings) {
            node_string = node_string.replace(" ", "");
            node_string = node_string.replace("\t", "");
            node_string = node_string.replace("[", "");
            node_string = node_string.replace("]", "");
            if (node_string != null && !node_string.equals("")) {
                Node node = new Node(node_string);
                list.add(node);
            }
        }

        return list;
    }

    public static String transformDAGNameFromPathDescriptor(String path_descriptor) {
        String dag_name = path_descriptor.replace("PATH..", "");
        dag_name = dag_name.substring(0, dag_name.indexOf("-"));
        return dag_name;
    }

    public static int transformPathIdFromPathDescriptor(String path_descriptor) {
        String path_id_st = path_descriptor.replace("PATH..", "");
        path_id_st = path_id_st.substring(path_id_st.indexOf("-") + 1);
        int path_id = Integer.parseInt(path_id_st);
        return path_id;
    }

    public static Path_Label transformPathLabelFromString(String plstring, boolean bear_sigm) {
        if (plstring.startsWith("set-")) {
            String pl_tr = plstring.replace("set-", "");
            String[] both_sides = pl_tr.split("_");
            String ptwcntstr = both_sides[0];
            String sigmcntstr = both_sides[1];
            int ptwcnt = Integer.parseInt(ptwcntstr);
            int sigmcnt = Integer.parseInt(sigmcntstr);
            Path_Label pl = new Path_Label(ptwcnt, sigmcnt, bear_sigm, new ArrayList());
            return pl;
        } else {
            plstring = plstring.replace("pathway-","");
            String[] both_sides = plstring.split("_");
            String ptwnm = both_sides[0];
            String sigmcntstr = both_sides[1];
            int ptwcnt = -1;
            int sigmcnt = Integer.parseInt(sigmcntstr);
            ArrayList<String> pathways = new ArrayList();
            pathways.add(ptwnm);
            Path_Label pl = new Path_Label(ptwcnt, sigmcnt, bear_sigm, pathways);
            return pl;
        }
    }

    /*
    OTHER FUNCTIONS
     */
    public static ArrayList revertArrayList(ArrayList list) {
        ArrayList new_arr = new ArrayList();
        for (int i = list.size() - 1; i >= 0; i--) {
            new_arr.add(list.get(i));
        }
        return new_arr;
    }

    public static String stringNTimes(String st, int N) {
        String to_ret = "";
        for (int i = 0; i < N; i++) {
            to_ret += st;
        }
        return to_ret;
    }

    public static ArrayList combineLists(ArrayList l1, ArrayList l2) {
        ArrayList arrl = new ArrayList();
        for (Object o : l1) {
            arrl.add(o);
        }
        for (Object o : l2) {
            arrl.add(o);
        }
        return arrl;
    }

    public static ArrayList copyList(ArrayList to_copy) {
        ArrayList new_arr = new ArrayList();
        for (Object o : to_copy) {
            new_arr.add(o);
        }
        return new_arr;
    }

    public static ArrayList listsDifference(ArrayList new_l, ArrayList prev_l) {
        ArrayList diff = new ArrayList();
        for (Object o : new_l) {
            if (!prev_l.contains(o)) {
                diff.add(o);
            }
        }
        return diff;
    }

    public static ArrayList<String> listFromString(String list_st) {
        String transf = list_st.replace("[", "").replace("]", "").replace(" ", "");
        String[] parts = transf.split(",");
        return new ArrayList(Arrays.asList(parts));
    }

    public static int percentFromBytesRead(long bytes_read, long bytes_to_read) {
        double portion = ((double) bytes_read) / ((double) bytes_to_read);
        int percent = (int) (portion * 100);
        // System.out.println(percent);
        return percent;
    }

    //
    public static int getTextPixelSize(String text, int mul_dim) {
        int char_pix = 12;
        if (text.equals("-")) {
            char_pix = 5;
        }
        return char_pix * text.length() * mul_dim;
    }

    public static String longestStringLength(ArrayList<String> strings) {
        int max_l = Integer.MIN_VALUE;
        String max_s = null;
        for (String st : strings) {
            if (st.length() > max_l) {
                max_l = st.length();
                max_s = st;
            }
        }
        return max_s;
    }

    public static String get5DigitsString(int number) {
        return String.format("%05d", number);
    }
}

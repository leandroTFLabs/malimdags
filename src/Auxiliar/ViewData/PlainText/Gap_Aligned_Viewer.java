/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auxiliar.ViewData.PlainText;

import Alignments.PathAlignments.PairWise.Aligning_Token;
import Auxiliar.Utils;
import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class Gap_Aligned_Viewer {
    private static String seprt = "";
    public static String visualizeAlignment(ArrayList<String>[] rows, ArrayList<Aligning_Token>[] gap_info) {
        String to_ret = "";
        int row_cnt = 0;
        int max_size = 4;
        //align text
        for (ArrayList<String> row : rows) {
            for (String part : row) {
                if (part.length()>=max_size) max_size = part.length();
            }
        }
        int increment_factor = max_size;
        String local_seprt = Utils.stringNTimes(seprt, increment_factor);
        //prepare text
        for (ArrayList row : rows) {
            int gap_cnt = 0;
            for (Aligning_Token tk : gap_info[row_cnt]) {
                if (tk.getGap()) {
                    to_ret += local_seprt+"-"+local_seprt;
                } else {
                    if (gap_cnt < row.size()) to_ret += local_seprt+row.get(gap_cnt)+local_seprt;
                    gap_cnt++;
                }
            }
            to_ret += "\r\n";
            row_cnt++;
        }
        return to_ret;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auxiliar.ViewData.PlainText;

import Alignments.PathAlignments.Multiple.PITools.Multiple_Alignment;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author ff
 */
public class Align_To_PlainFile {
    /*
    ATT
    */
    //alignments
    private final String filename;
    private final ArrayList<String> ndescript;
    private final Multiple_Alignment ma;
    /*
    CONSTRUCTION
    */
    public Align_To_PlainFile(String filename, ArrayList<String> ndescript, Multiple_Alignment ma, boolean focused, Object focus_obj) throws IOException {
        this.filename = filename;
        this.ndescript = ndescript;
        this.ma = ma;
        constructFile();
    }

    private void constructFile() throws IOException {
        File f = new File(filename);
        try (FileWriter fw = new FileWriter(f); BufferedWriter bw = new BufferedWriter(fw)) {
            ArrayList<ArrayList<String>> elems_descriptions;
            elems_descriptions = ma.getElemsDescriptions();
            double[] column_scores = ma.getColumnScores();
            String formatted_scores_label = String.format("%1$-50s", "Column Scores -->");
            String formatted_column_scores = Arrays.toString(column_scores);
            bw.write(formatted_scores_label+formatted_column_scores+"\r\n");
            for (int i = 0; i < elems_descriptions.size(); i++) {
                String formatted_path_name = String.format("%1$-50s", ndescript.get(i));
                String formatted_row_elems = elems_descriptions.get(i).toString();
                bw.write(formatted_path_name+formatted_row_elems+"\r\n");
            }
        }
    }
}

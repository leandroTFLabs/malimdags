/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SpinalPrograms;

import Alignments.PathAlignments.Multiple.MA_Group_Aligner;
import Auxiliar.Comparison.MBB_Simil_Data;
import Auxiliar.Utils;
import Forest.Path;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author ASUS
 */
public class PathMAlignment {

    private final boolean use_huge_memory = true;
    private final double min_score = 0.5;
    
    private final HashMap<String, Path> memory_load = new HashMap<>();

    private String hashCodeForLoad(String dag_name, String path_desc) {
        return dag_name+"--"+path_desc;
    }

    public void loadDagPathsOnMemory(String dag_name) throws IOException {
        System.out.println("Loading paths from "+dag_name+" at memory...");
        String currentDir = System.getProperty("user.dir");
        String pathwysDir = currentDir.concat("/DATA/calculated/extracted-paths/");
        File fp = new File(pathwysDir.concat(dag_name).concat(".txt"));
        
        FileReader fr = new FileReader(fp);
        BufferedReader br = new BufferedReader(fr);
        
        String line = br.readLine();
        // Path foundp = null;
        while (line != null && !line.replace("\t", "").replace(" ", "").equals("")) {
            if (line.startsWith("/")) {
                Path p = Utils.processPathLine(line,true);
                String descriptor = p.getDescriptor();
                this.memory_load.put(hashCodeForLoad(dag_name,descriptor),p);
            }
            
            line = br.readLine();
        }
        br.close();
    }

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     * @throws java.lang.CloneNotSupportedException
     */
    public static void main(String[] args) throws IOException, CloneNotSupportedException {
        new PathMAlignment().run();
    }

    public void run() throws IOException, CloneNotSupportedException {
        ArrayList<MA_Group_Aligner> alignment_threads = new ArrayList<>();
        String currentDir = System.getProperty("user.dir");
        
        //we clean previous alignments
        String ma_dir_name = System.getProperty("user.dir").concat("/DATA/calculated/MULTIPLE-ALIGNMENTS/");
        File ma_dir = new File(ma_dir_name);
        ma_dir.mkdir();
        for (File f: ma_dir.listFiles()) {
            System.out.println("Deleting file "+f.getName());
            try {
                if (!f.delete()) {
                    for (File nested : f.listFiles()) {
                        nested.delete();
                    }
                    f.delete();
                };
                System.out.println("Deleted");
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }

        // System.out.println(currentDir);
        MBB_Simil_Data mbbsd = new MBB_Simil_Data();
        String maReadyDir = currentDir.concat("/DATA/calculated/MA-ready-alignments/MA-ready-alignments.txt");
        File maF = new File(maReadyDir);
        try (FileReader mafr = new FileReader(maF)) {
            BufferedReader mabr = new BufferedReader(mafr);

            String curr_gr_desc = "none";

            String line = mabr.readLine();
            String[] dag_names = line.replace("\t", " ").split(" ");
            if (this.use_huge_memory) {
                for (String dag : dag_names) {
                    this.loadDagPathsOnMemory(dag);
                }
            }
            // System.out.println(Arrays.toString(dag_names));
            line = mabr.readLine();
            String[] current_dag_names = new String[dag_names.length];
            ArrayList<ArrayList<Path>> gr_paths = new ArrayList<>();
            while (line != null && !line.replace(" ", "").replace("\t", "").equals("")) {
                // System.out.println("New Line");
                if (line.startsWith("pathway-") || line.startsWith("set")) {
                    // set
                    if (!curr_gr_desc.equals("none")) {
                        MA_Group_Aligner ma_Group_Aligner = new MA_Group_Aligner(curr_gr_desc, gr_paths, mbbsd);
                        ma_Group_Aligner.start();
                        alignment_threads.add(ma_Group_Aligner);
                        gr_paths = new ArrayList<>();
                    }
                    curr_gr_desc = line;
                } else if (line.startsWith("p")) {
                    // path line
                    if (scoreEnough(line)) {
                        ArrayList<Path> path_line = extractPathsFromLine(current_dag_names, line, curr_gr_desc.contains("_"));
                        gr_paths.add(path_line);
                    }
                } else if (line.startsWith("[")) {
                    current_dag_names = line.replace("[","").replace("]","").replace(" ","").replace("\t","").split(",");
                }

                line = mabr.readLine();
            }

            if (!curr_gr_desc.equals("none")) {
                MA_Group_Aligner ma_Group_Aligner = new MA_Group_Aligner(curr_gr_desc, gr_paths, mbbsd);
                ma_Group_Aligner.start();
                alignment_threads.add(ma_Group_Aligner);
            }
        }
        
        for (MA_Group_Aligner t : alignment_threads) {
            try {
                t.join();
            } catch (InterruptedException ex) {
                
            }
        }
    }

    private boolean scoreEnough(String line) {
        String[] parts = line.replace("\t", " ").split(" ");
        return Double.parseDouble(parts[parts.length - 1].replace("score=", "")) > min_score;
    }

    private ArrayList<Path> extractPathsFromLine(String[] dag_names, String line, boolean bear_sigm)
            throws IOException {
        ArrayList<Path> paths = new ArrayList<>();
        String[] parts = line.replace("\t", " ").split(" ");
        // System.out.println(Arrays.toString(parts));
        for (int i = 0; i < dag_names.length; i++) {
            String dag_name = dag_names[i];
            try {
                String path_desc = parts[i];
                if (path_desc.startsWith("p")) {
                    Path p;
                    if (!this.use_huge_memory) p = Utils.findPath(dag_name, path_desc, bear_sigm);
                    else p = this.memory_load.get(this.hashCodeForLoad(dag_name, path_desc));
                    if (p != null)
                        paths.add(p);
                }
            } catch (Exception ex) {
            }
        }
        return paths;
    }
}

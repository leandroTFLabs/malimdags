/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SpinalPrograms;

import Forest.DAG;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class PathExtraction {
    /*
    PARAMS
    */
    private boolean bear_sigm = true;
    private double minimum_sigm = 0.5;
    private int minimum_length = 4;
    //
    
    public static void main(String[] args) throws IOException {
        new PathExtraction().run();
    }
    
    private ArrayList<DAG> dags_identified;
    
    public void run() throws IOException {
        String currentDir = System.getProperty("user.dir");
        String pathwysDir = currentDir.concat("/DATA/DAGs_to_process/");
        File f = new File(pathwysDir);
        dags_identified = new ArrayList();
        for (File file : f.listFiles()) {
            if (!file.isDirectory()) {
                //System.out.println("Found file called "+file.getName());
                String dag_name = file.getName().replace(".dot", "");
                DAG new_dag = new DAG(dag_name);
                new_dag.extractFromFile(bear_sigm,minimum_sigm,minimum_length);
                new_dag.dumpIntoFile();
                dags_identified.add(new_dag);
            }
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SpinalPrograms;

import Alignments.DAGAlignments.Labeled_Alignment;
import Alignments.DAGAlignments.Labeled_Structure;
import Alignments.DAGAlignments.MT_DAG_Aligner;
import Disk_Mgmt.Structured_Paths_File;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;

/**
 *
 * @author ASUS
 */
public class PathTreatment {
    /*
    PARAMS
    */
    private boolean bear_sigm = false;
    private int minimum_length = 5;
    private int max_sample_size = 1000;
    //
    
    public class Str_File_Compare implements Comparator<Structured_Paths_File> {

        @Override
        public int compare(Structured_Paths_File o1, Structured_Paths_File o2) {
            return o1.getPathsSize() != o2.getPathsSize() ? Integer.compare(o2.getPathsSize(), o1.getPathsSize()) : o1.getDAGName().compareTo(o2.getDAGName());
        }
        
    }
    
    public static void main(String[] args) throws IOException {
        new PathTreatment().run();
    }
    
    private ArrayList<Structured_Paths_File> files_identified;
    private ArrayList<String> dag_names;
    private ArrayList<Labeled_Structure> structs;
    private Labeled_Alignment result;
    
    public void run() throws IOException {
        String currentDir = System.getProperty("user.dir");
        String pathwysDir = currentDir.concat("/DATA/calculated/extracted-paths/");
        File f = new File(pathwysDir);
        files_identified = new ArrayList<>();
        for (File file : f.listFiles()) {
            if (!file.isDirectory()) {
                //System.out.println("Found file called "+file.getName());
                String dag_name = file.getName().replace(".txt", "");
                Structured_Paths_File spf = new Structured_Paths_File(dag_name,file,bear_sigm,minimum_length,max_sample_size);
                this.files_identified.add(spf);
            }
        }
        this.files_identified.sort(new Str_File_Compare());
        this.structs = new ArrayList<>();
        this.dag_names = new ArrayList<>();
        for (Structured_Paths_File spf : this.files_identified) {
            spf.inspectLabeledStructure();
            this.dag_names.add(spf.getDAGName());
            this.structs.add(spf.getLabeledStructure());
        }
        MT_DAG_Aligner align = new MT_DAG_Aligner(dag_names,structs,max_sample_size);
        this.result = align.getFinalAlignment();
        this.result.dumpIntoFile(dag_names, bear_sigm);
    }
}

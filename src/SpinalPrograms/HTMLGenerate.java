/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SpinalPrograms;

import HTMLFormat.MultipleAlignmentHTMLFormatter;
import QueryTool.querying.Query_Data;
import java.io.IOException;

/**
 *
 * @author ff
 */
public class HTMLGenerate {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        new HTMLGenerate().run();
    }

    public void run() throws IOException {
        Query_Data qd = new Query_Data();
        MultipleAlignmentHTMLFormatter formatter = new MultipleAlignmentHTMLFormatter(qd);
    }
    
}

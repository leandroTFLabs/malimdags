/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Concurrent;

import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class Task_Divider {
    
    private ArrayList tasks;
    private int n_threads;
    private ArrayList[] divided_tasks;
    private int list_tam;
    private int num_edges;
    private int edges_tam;
    
    public Task_Divider(int n_threads, ArrayList tasks) {
        this.tasks = tasks;
        this.n_threads = n_threads;
        this.list_tam = Math.min(tasks.size()-1,this.n_threads);
        this.divided_tasks = new ArrayList[list_tam];
        divideTasks();
    }
    private void divideTasks() {
        num_edges = tasks.size()-1;
        edges_tam = (int) Math.ceil(num_edges/list_tam);
        int cnt = 0;
        while (cnt < num_edges) {
            int index = indexForEdge(cnt);
            Object[] edge = getEdgeStructures(cnt);
            for (Object task : edge) {
                insertToIndex(task,index);
            }
            cnt++;
        }
    }
    private int indexForEdge(int n_edge) {
        return n_edge/edges_tam;
    }
    private Object[] getEdgeStructures(int n_edge) {
        Object[] to_ret = new Object[2];
        to_ret[0] = tasks.get(n_edge);
        to_ret[1] = tasks.get(n_edge+1);
        return to_ret;
    }
    private void insertToIndex(Object task, int index) {
        ArrayList inner = new ArrayList();
        if (divided_tasks[index] != null) {
            inner = divided_tasks[index];
        }
        inner.add(task);
        divided_tasks[index] = inner;
    }
    public ArrayList[] getDividedLists() {
        return divided_tasks;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package malimdags;

import Alignments.DAGAlignments.Hungarian_Algorithm;
import Auxiliar.Comparison.MBB_Simil_Data;
import Auxiliar.Utils;
import Disk_Mgmt.Class_Labeler;
import Disk_Mgmt.MBB_Labeler;
import Forest.Path;
import Disk_Mgmt.MBB_Reactions;
import QueryTool.querying.Open_Links;
import SpinalPrograms.PathExtraction;
import SpinalPrograms.PathMAlignment;
import SpinalPrograms.PathTreatment;
import SpinalPrograms.HTMLGenerate;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class MalimDAGs {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     * @throws java.lang.CloneNotSupportedException
     */
    public static void main(String[] args) throws IOException, CloneNotSupportedException {
        new MalimDAGs().run();
    }
    
    public void run() {
        try {
            int total_time_secs = 0;
            int total_time_mins = 0;

            long ctime = 0;
            long ntime = 0;
            long esecs = 0;
            long emins = 0;

             ctime = System.currentTimeMillis();
             new PathExtraction().run();
             ntime = System.currentTimeMillis();
             esecs = (ntime - ctime)/1000;
             emins = esecs/60;
             total_time_secs += esecs;
             total_time_mins += emins;
             System.out.println("Time for paths extraction: "+emins+"min = "+esecs+"s");
            
             ctime = System.currentTimeMillis();
             new PathTreatment().run();
             ntime = System.currentTimeMillis();
             esecs = (ntime - ctime)/1000;
             emins = esecs/60;
             total_time_secs += esecs;
             total_time_mins += emins;
             System.out.println("Time for cluster treatment: "+emins+"min = "+esecs+"s");
            
            ctime = System.currentTimeMillis();
            new PathMAlignment().run();
            ntime = System.currentTimeMillis();
            esecs = (ntime - ctime)/1000;
            emins = esecs/60;
            total_time_secs += esecs;
            total_time_mins += emins;
            System.out.println("Time for multiple alignment: "+emins+"min = "+esecs+"s");

            ctime = System.currentTimeMillis();
            new HTMLGenerate().run();
            ntime = System.currentTimeMillis();
            esecs = (ntime - ctime)/1000;
            emins = esecs/60;
            total_time_secs += esecs;
            total_time_mins += emins;
            System.out.println("Time for HTML generation: "+emins+"min = "+esecs+"s");

            System.out.println("Total Time Taken: "+total_time_mins+"min = "+total_time_secs+"s");
            
            //Open_Links.open_pathway("01100");
            
            //new Verifications().run();
            
            /*Class_Labeler cl = new Class_Labeler(false);
            System.out.println(cl.getLabelDescription("75_7"));*/
            
            /*MBB_Simil_Data simils = new MBB_Simil_Data();
            System.out.println(simils.getSimilMBBs("1287", "1287"));*/
            
            /*
            Hungarian_Algorithm ha = new Hungarian_Algorithm(new float[][]{{1f,0f},{0f,1f}});
            System.out.println(Arrays.toString(ha.findOptimalAssignment()[0]));
            System.out.println(Arrays.toString(ha.findOptimalAssignment()[1]));
            */
        } catch (Exception ex) {
            System.out.println(ex);
            Logger.getLogger(MalimDAGs.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}

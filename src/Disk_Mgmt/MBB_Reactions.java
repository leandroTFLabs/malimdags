/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Disk_Mgmt;

import Auxiliar.Utils;
import QueryTool.querying.Query_Data;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author ASUS
 */
public class MBB_Reactions {
    private final String experiment = "GUT";

    private final String mbb_info_path = "/DATA/rctns_MBBs/results_"+experiment+".csv";
    private HashMap<String,ArrayList<String>> mbb_rtns_map = new HashMap();
    
    private Query_Data invoker;
    
    public MBB_Reactions(Query_Data invoker) throws FileNotFoundException, IOException {
        
        this.invoker = invoker;
        
        String currentDir = System.getProperty("user.dir");
        String abs_path = currentDir.concat(mbb_info_path);
        File f = new File(abs_path);
        FileReader fr = new FileReader(f);
        BufferedReader br = new BufferedReader(fr);
        
        ArrayList<String> cols = new ArrayList();
        
        long bytes_to_read = f.length();
        long bytes_read = 0;
        
        String first_line = br.readLine();
        while (first_line != null && !first_line.equals("")) {
            boolean delim = first_line.contains(",");
            String this_rtn = delim? first_line.substring(0,first_line.indexOf(",")) : first_line;
            this_rtn = this_rtn.contains("(") ? this_rtn.substring(0,this_rtn.indexOf("(")) : this_rtn;
            this_rtn = this_rtn.replace(" ", "").replace("\t", "").replace("\"","");
            cols.add(this_rtn);
            first_line = delim ? first_line.substring(first_line.indexOf(",")+1) : "";
        }
        
        String line = br.readLine();
        int gen_cnt = 0;
        while (line != null && !line.equals("")) {
            
            bytes_read += line.length();
            
            String key = line.substring(0,line.indexOf(","));
            key = key.replace("\"","");
            String line_aux = line.substring(line.indexOf(",")+1);
            int cnt = 0;
            while (line_aux != null && !line_aux.equals("")) {
                boolean delim = line_aux.contains(",");
                String this_val = delim? line_aux.substring(0,line_aux.indexOf(",")) : line_aux;
                this_val = this_val.replace(" ", "").replace("\t", "").replace("\"","");
                String key_n = cols.get(cnt+1);
                ArrayList<String> nested = mbb_rtns_map.containsKey(this_val) ? mbb_rtns_map.get(this_val) : new ArrayList();
                if (!nested.contains(key_n)) nested.add(key_n);
                mbb_rtns_map.put(this_val,nested);
                line_aux = delim ? line_aux.substring(line_aux.indexOf(",")+1) : "";
                cnt++;
            }
            
            line = br.readLine();
            gen_cnt++;
        }
        fr.close();
    }
    public ArrayList<String> findMBBReactions(String mbb_bio_id) {
        return mbb_rtns_map.get(mbb_bio_id);
    }
}

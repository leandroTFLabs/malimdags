/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Disk_Mgmt;

import Forest.DAG;
import Forest.Path;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author ASUS
 */
public class DAG_Paths_Dumper {
    
    private DAG to_dump;
    
    public DAG_Paths_Dumper(DAG to_dump) {
        this.to_dump = to_dump;
    }
    
    public void dump_DAG_paths() throws IOException {
        //aquí, s'escriuran els camins extrets a un fitxer
        String currentDir = System.getProperty("user.dir");
        String pathwysDir = currentDir.concat("/DATA/");
        String resultsDir = pathwysDir.concat("/calculated/extracted-paths/");
        String dag_paths_file = to_dump.getDagName()+".txt";
        File f = new File(resultsDir);
        f.mkdir();
        //we clean the dir
        for (File file : f.listFiles()) {
            if (!file.isDirectory()) {
                f.delete();
            }
        }
        //paths file name
        String paths_file_name = resultsDir.concat(dag_paths_file);
        
        try (FileWriter writer = new FileWriter(paths_file_name);
            BufferedWriter bw = new BufferedWriter(writer);) {
            
            bw.write(to_dump.toString());
            
            for (Path p : to_dump.getDagPaths()) {
                bw.write("/PATH.."+p.toString()+"/\t/size="+p.getPathSize()+"/\t/"+p.getEtiqCami()+"/\t");
                bw.write("\t"+p.getNodesCami().toString()+"\r\n");
            }
        }
    }
}

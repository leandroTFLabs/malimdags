/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Disk_Mgmt;

import Alignments.DAGAlignments.Labeled_Structure;
import Auxiliar.Clustering.Path_Label;
import Auxiliar.Utils;
import Auxiliar.Clustering.Label_Limiter;
import Forest.Path;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author ASUS
 */
public class Structured_Paths_File {
    
    private final int META_DATA_LINES_SIZE = 27;
    private boolean bear_sigm = true;
    private int minimum_length;
    private int max_sample_size;
    private Label_Limiter ll;
    
    private String DAG_name;
    private File paths_file;
    private int paths_size;
    private Labeled_Structure dag_paths_str;
    
    public Structured_Paths_File(String DAG_name, File paths_file, boolean bear_sigm, int minimum_length, int max_sample_size) throws FileNotFoundException, IOException {
        dag_paths_str = new Labeled_Structure();
        this.paths_file = paths_file;
        this.DAG_name = DAG_name;
        this.bear_sigm = bear_sigm;
        this.minimum_length = minimum_length;
        this.max_sample_size = max_sample_size;
        this.ll = new Label_Limiter(max_sample_size);
        inspectPathsSize();
    }
    
    private void inspectPathsSize() throws FileNotFoundException, IOException {
        FileReader fr = new FileReader(paths_file);
        BufferedReader br = new BufferedReader(fr);
        String first_line = br.readLine();
        
        first_line = first_line.replace("S'han extret un total de ","");
        first_line = first_line.replace(" camins","");
        
        paths_size = Integer.parseInt(first_line);
        
        fr.close();
    }
    
    public void inspectLabeledStructure() throws FileNotFoundException, IOException {
        //System.out.println("New DAG");
        FileReader fr = new FileReader(paths_file);
        BufferedReader br = new BufferedReader(fr);
        
        String line = null;
        for (int i = 0; i < META_DATA_LINES_SIZE; i++) {
            line = br.readLine();
        }
        
        line = br.readLine();
        while (line.startsWith("*")) {
            line = br.readLine();
        }
        
        //linies de camins
        //line = br.readLine();
        Path_Label last_pl = null;
        while (line != null && !line.equals("\n") && !line.equals("") && !line.equals(" ") && !line.equals("\t")) {
            Path new_path = Utils.processPathLine(line,bear_sigm);
            Path_Label pl = new_path.getEtiqCami();
            //System.out.println(pl == last_pl);
            last_pl = pl;
            ll.incrementLabel(pl);
            if (!ll.exceedsLimit(pl)&&new_path.getPathSize()>=minimum_length) dag_paths_str.addNewPath(pl, new_path);
            line = br.readLine();
        }
        
        fr.close();
    }
    
    public String getDAGName() {
        return DAG_name;
    }
    public int getPathsSize() {
        return paths_size;
    }
    public Labeled_Structure getLabeledStructure() {
        return dag_paths_str;
    }
}

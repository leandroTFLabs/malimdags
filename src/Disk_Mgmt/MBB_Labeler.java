/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Disk_Mgmt;

import Auxiliar.Utils;
import QueryTool.querying.Query_Data;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

/**
 *
 * @author ASUS
 */
public class MBB_Labeler {
    private HashMap<String,String> bio_2_label_map;
    private HashMap<String,String> label_2_bio_map;
    private final String SEPR = "---";
    private int cnt = 0;
    //
    private File labeledF;
    
    //invoker
    private Query_Data invoker;
    
    public MBB_Labeler(Query_Data invoker) throws FileNotFoundException, IOException {
        this.invoker = invoker;
        String currentDir = System.getProperty("user.dir");
        String labeledFName = currentDir.concat("/DATA/calculated/labeled-MBBs/labeled-MBBs.txt");
        this.labeledF = new File(labeledFName);
        this.bio_2_label_map = new HashMap();
        this.label_2_bio_map = new HashMap();
        if (labeledF.exists()) loadMBBData();
    }
    private void loadMBBData() throws FileNotFoundException, IOException {
        FileReader fr = new FileReader(labeledF);
        
        long bytes_to_read = labeledF.length();
        long bytes_read = 0;
        
        BufferedReader br = new BufferedReader(fr);
        String line = br.readLine();
        while (line != null && !line.replace(" ","").replace("\t","").equals("")) {
            
            bytes_read += line.length();
            
            String tr_line = line.replace(" ","").replace("\t","");
            processLine(tr_line);
            cnt++;
            
            line = br.readLine();
        }
        fr.close();
    }
    private void processLine(String line) {
        String[] parts = line.split(SEPR);
        String lab = parts[0];
        String bio_id = parts[1];
        label_2_bio_map.put(lab, bio_id);
        bio_2_label_map.put(bio_id, lab);
    }
    private String generateNewLabel() {
        String label = "MBBL".concat(Utils.get5DigitsString(cnt));
        cnt++;
        return label;
    }
    private String addMBBLabel(String bio_id) {
        String gen = generateNewLabel();
        label_2_bio_map.put(gen, bio_id);
        bio_2_label_map.put(bio_id, gen);
        return gen;
    }
    private String generateLine(String label, String bio_id) {
        return label + "\t"+SEPR+"\t" + bio_id + "\r\n";
    }
    //
    public String getMBBLabel(String bio_id) {
        if (bio_2_label_map.containsKey(bio_id)) {
            return bio_2_label_map.get(bio_id);
        } else {
            return addMBBLabel(bio_id);
        }
    }
    public String getMBBBioId(String mbb_label) {
        return label_2_bio_map.get(mbb_label);
    }
    public void dumpIntoFile() throws IOException {
        FileWriter fw = new FileWriter(labeledF);
        BufferedWriter bw = new BufferedWriter(fw);
        for (int i = 0; i < cnt; i++) {
            String lab = "MBBL" + Utils.get5DigitsString(i);
            if (label_2_bio_map.containsKey(lab)) bw.write(generateLine(lab,label_2_bio_map.get(lab)));
            bw.flush();
        }
        fw.close();
    }
}

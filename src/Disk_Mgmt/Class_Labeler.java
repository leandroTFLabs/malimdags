/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Disk_Mgmt;

import Auxiliar.Clustering.Path_Label;
import Auxiliar.Clustering.Path_Class;
import Auxiliar.Utils;
import QueryTool.querying.Query_Data;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

/**
 *
 * @author ASUS
 */
public class Class_Labeler {

    private class Name_Counter {
        private String name;
        private int counter;

        public Name_Counter(String name, int counter) {
            this.name = name;
            this.counter = counter;
        }

        public String getName() {
            return name;
        }

        public int getCounter() {
            return counter;
        }
    }

    private class Comparador_Name_Counter implements Comparator<Name_Counter> {
        @Override
        public int compare(Name_Counter o1, Name_Counter o2) {
            return Integer.compare(o1.getCounter(), o2.getCounter());
        }
    }

    private HashMap<String, HashMap<String, Path_Label>> class_labels;
    private HashMap<Integer, HashMap<Integer, String>> rev_class_labels;
    private HashMap<String, ArrayList<Name_Counter>> sig_lab_list_map;

    private HashMap<String, Integer> mono_cnt_map;
    private HashMap<Integer, String> rev_mono_map;
    private ArrayList<Name_Counter> ptw_lab_list;

    private File labeledF;

    private boolean bear_sigm;

    private Query_Data invoker;

    public Class_Labeler(boolean bear_sigm, Query_Data invoker) throws FileNotFoundException, IOException {
        String currentDir = System.getProperty("user.dir");
        String labeledFName = currentDir.concat("/DATA/calculated/labeled-classes/labeled-classes.txt");
        this.labeledF = new File(labeledFName);
        this.class_labels = new HashMap();
        this.rev_class_labels = new HashMap();
        this.mono_cnt_map = new HashMap();
        this.rev_mono_map = new HashMap();
        this.ptw_lab_list = new ArrayList();
        this.sig_lab_list_map = new HashMap();
        this.bear_sigm = bear_sigm;
        loadLabelData();
    }

    private String processLine(String line) {
        return line == null ? null : line.replace("\t", "").replace(" ", "").replace("\r\n", "");
    }

    public void loadLabelData() throws FileNotFoundException, IOException {
        if (labeledF.exists()) {
            // recorrem les línies identificant els comptadors pathways i sigma
            FileReader fr = new FileReader(labeledF);
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            line = processLine(line);
            String ptw_list = "";
            String sig_list = "";
            int ptw_cnt = -1;
            int sig_cnt = -1;

            long bytes_to_read = labeledF.length();
            long bytes_read = 0;

            int gen_cnt = 0;
            while (line != null && !line.equals("")) {

                bytes_read += line.length();

                String[] two_sides = line.split("/");
                String cnt_part = two_sides[0];
                int cnt = Integer.parseInt(cnt_part);
                String dat_part = two_sides[1];
                boolean ptw_dat = dat_part.startsWith("ptw");
                if (ptw_dat) {
                    ptw_list = dat_part.replace("ptw", "");
                    ptw_cnt = cnt;
                    if (!mono_cnt_map.containsKey(ptw_list)) {
                        class_labels.put(ptw_list, new HashMap());
                        rev_class_labels.put(ptw_cnt, new HashMap());
                        rev_mono_map.put(ptw_cnt, ptw_list);
                        sig_lab_list_map.put(ptw_list, new ArrayList());
                        mono_cnt_map.put(ptw_list, ptw_cnt);
                        ptw_lab_list.add(new Name_Counter(ptw_list, ptw_cnt));
                    }
                } else {
                    sig_list = dat_part.replace("sig", "");
                    sig_cnt = cnt;
                    ArrayList<String> ptwlist_arr = new ArrayList();
                    String[] copy = ptw_list.replace("[", "").replace("]", "").split(",");
                    for (String pw : copy) {
                        ptwlist_arr.add(pw);
                    }
                    Path_Label pl = new Path_Label(ptw_cnt, sig_cnt, bear_sigm, ptwlist_arr);
                    HashMap<String, Path_Label> nested = class_labels.get(ptw_list);
                    nested.put(sig_list, pl);
                    class_labels.put(ptw_list, nested);

                    // System.out.println(ptw_cnt);

                    HashMap<Integer, String> rev_nested = rev_class_labels.get(ptw_cnt);
                    rev_nested.put(sig_cnt, sig_list);
                    rev_class_labels.put(ptw_cnt, rev_nested);

                    ArrayList<Name_Counter> nested_list = sig_lab_list_map.get(ptw_list);
                    nested_list.add(new Name_Counter(sig_list, sig_cnt));
                    sig_lab_list_map.put(ptw_list, nested_list);
                }
                line = br.readLine();
                line = processLine(line);
                gen_cnt++;
            }
            fr.close();
        }
    }

    public Path_Label getClassLabel(Path_Class classe) {
        String ptw_list = classe.getClassesLiders().toString();
        ptw_list = ptw_list.replace("\t", "").replace(" ", "");
        String sig_list = classe.getSigmesLiders().get(0).toString();
        if (mono_cnt_map.containsKey(ptw_list)) {
            int ptw_cnt = mono_cnt_map.get(ptw_list);
            HashMap<String, Path_Label> nested = class_labels.get(ptw_list);
            if (nested.containsKey(sig_list)) {
                return nested.get(sig_list);
            } else {
                int sig_cnt = nested.keySet().size();
                Path_Label to_ret = new Path_Label(ptw_cnt, sig_cnt, bear_sigm, classe.getClassesLiders());
                nested.put(sig_list, to_ret);
                class_labels.put(ptw_list, nested);

                HashMap rev_nested = rev_class_labels.get(ptw_cnt);
                rev_nested.put(sig_cnt, sig_list);
                rev_class_labels.put(ptw_cnt, rev_nested);

                ArrayList<Name_Counter> nested_list = sig_lab_list_map.get(ptw_list);
                nested_list.add(new Name_Counter(sig_list, sig_cnt));
                sig_lab_list_map.put(ptw_list, nested_list);
                return to_ret;
            }
        } else {
            int ptw_cnt = mono_cnt_map.keySet().size();
            mono_cnt_map.put(ptw_list, ptw_cnt);

            rev_mono_map.put(ptw_cnt, ptw_list);

            ptw_lab_list.add(new Name_Counter(ptw_list, ptw_cnt));
            int sig_cnt = 0;
            Path_Label to_ret = new Path_Label(ptw_cnt, sig_cnt, bear_sigm, classe.getClassesLiders());
            HashMap nested = new HashMap();
            nested.put(sig_list, to_ret);
            class_labels.put(ptw_list, nested);

            HashMap rev_nested = new HashMap();
            rev_nested.put(sig_cnt, sig_list);
            rev_class_labels.put(ptw_cnt, rev_nested);

            ArrayList<Name_Counter> nested_list = new ArrayList();
            nested_list.add(new Name_Counter(sig_list, sig_cnt));
            sig_lab_list_map.put(ptw_list, nested_list);
            return to_ret;
        }
    }

    public String getLabelDescription(String label_name) {
        String to_ret = "";
        Path_Label pl = Path_Label.getPlFromName(label_name, bear_sigm);
        int ptwcnt = -1;
        int sigcnt = -1;
        if (pl.getPathway() != null && !pl.getPathway().equals("")) {
            ptwcnt = mono_cnt_map.get("[" + pl.getPathway() + "]");
            sigcnt = pl.getSigCnt();
        } else {
            ptwcnt = pl.getPtwCnt();
            sigcnt = pl.getSigCnt();
        }
        to_ret = to_ret.concat(rev_mono_map.get(ptwcnt));
        if (bear_sigm) {
            to_ret = to_ret.concat("\t-\t" + rev_class_labels.get(ptwcnt).get(sigcnt));
        }
        return to_ret;
    }

    private String convertLines(String ptw_list) {
        int ptw_cnt = mono_cnt_map.get(ptw_list);
        String ptw_line = ptw_cnt + "/ptw" + ptw_list + "\r\n";
        String sig_lines = "";
        ArrayList<Name_Counter> list = sig_lab_list_map.get(ptw_list);
        list.sort(new Comparador_Name_Counter());
        for (Name_Counter nc : sig_lab_list_map.get(ptw_list)) {
            String sig_list = nc.getName();
            Path_Label pl = class_labels.get(ptw_list).get(sig_list);
            int sig_cnt = pl.getSigCnt();
            String sig_line = "\t\t" + sig_cnt + "/sig" + "\t" + sig_list + "\r\n";
            sig_lines += sig_line;
        }
        return ptw_line + sig_lines;
    }

    public void dumpIntoFile() throws IOException {
        FileWriter fw = new FileWriter(labeledF);
        BufferedWriter bw = new BufferedWriter(fw);
        ptw_lab_list.sort(new Comparador_Name_Counter());
        for (Name_Counter nc : ptw_lab_list) {
            String ptw_list = nc.getName();
            String to_write = convertLines(ptw_list);
            bw.write(to_write);
            bw.flush();
        }
        fw.close();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Disk_Mgmt;

import Forest.Node;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author ASUS
 */
public class MBB_Classifier {
    private final String experiment = "GUT";

    private ArrayList<String> keys;
    private HashMap<String,ArrayList<String>> dades_pathways;
    
    public MBB_Classifier() throws FileNotFoundException, IOException {
        String currentDir = System.getProperty("user.dir");
        String differF = currentDir.concat("/DATA/ptws_MBBs/MBB_Pw_relation_"+experiment+".csv");
        File f = new File(differF);
        FileReader fr = new FileReader(f);
        BufferedReader br = new BufferedReader(fr);
        this.keys = new ArrayList();
        this.dades_pathways = new HashMap();
        String first_line = br.readLine();
        first_line = first_line.substring(first_line.indexOf(",")+1);
        while (first_line != null && !first_line.equals("")) {
            boolean delim = first_line.contains(",");
            String this_mbb = delim? first_line.substring(0,first_line.indexOf(",")) : first_line;
            this_mbb = this_mbb.replace(" ", "").replace("\t", "").replace("\"","");
            keys.add(this_mbb);
            first_line = delim ? first_line.substring(first_line.indexOf(",")+1) : "";
        }
        String line = br.readLine();
        while (line != null && !line.equals("")) {
            String key = line.substring(0,line.indexOf(",")).replace("\"", "");
            String line_aux = line.substring(line.indexOf(",")+1);
            int cnt = 0;
            while (line_aux != null && !line_aux.equals("") && !line_aux.startsWith("\"[")) {
                boolean delim = line_aux.contains(",");
                String this_val = delim? line_aux.substring(0,line_aux.indexOf(",")) : line_aux;
                if (!this_val.equals("true") && !this_val.equals("false")) {
                    this_val = this_val.replace(" ", "").replace("\t", "").replace("\"","");
                    int val = Integer.parseInt(this_val);
                    String key_n = keys.get(cnt);
                    if (!key_n.equals("#Reactions") && !key_n.equals("#Pathways")) {
                        ArrayList<String> nested = dades_pathways.get(key) == null ? new ArrayList() : dades_pathways.get(key);
                        if (val == 1) {
                            nested.add(key_n);
                            dades_pathways.put(key,nested);
                        }
                    }
                }
                line_aux = delim ? line_aux.substring(line_aux.indexOf(",")+1) : "";
                cnt++;
            }
            line = br.readLine();
        }
        fr.close();
    }
    public ArrayList<String> getConjuntMBB(Node node) {
        ArrayList<String> pathways = dades_pathways.get(node.getBioId());
        return pathways;
    }
}

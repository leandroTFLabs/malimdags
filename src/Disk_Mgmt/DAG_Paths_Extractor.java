/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Disk_Mgmt;

import Auxiliar.Clustering.Path_Classifier;
import Auxiliar.Clustering.Path_Label;
import Auxiliar.Clustering.Label_Limiter;
import Forest.Node;
import Forest.Path;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public final class DAG_Paths_Extractor {
    ///////////////////////////////////////////////
    /*
     * ATRIBUTS
     */
    private MBB_Classifier dades_conjunts_pathways;
    private Class_Labeler etiquetador_camins;
    private MBB_Labeler mbb_labeler;
    private int index_actual;
    private ArrayList<Node> nodes_DAG;
    private ArrayList<Integer> indexos_arrels;
    private ArrayList<Node> cami_actual;
    private int id_actual;
    private ArrayList<Path> camins_extrets;
    private int profunditat;

    // PARAMS
    private boolean bear_sigm;
    private double minimum_sigm;
    private int minimum_length;
    //

    private Label_Limiter label_info = new Label_Limiter(Integer.MAX_VALUE);

    ///////////////////////////////////////////////
    /*
     * CONSTRUCTORS
     */
    public DAG_Paths_Extractor(String filename, boolean bear_sigm, double minimum_sigm, int minimum_length)
            throws IOException {
        this.bear_sigm = bear_sigm;
        this.minimum_sigm = minimum_sigm;
        this.minimum_length = minimum_length;
        //
        etiquetador_camins = new Class_Labeler(this.bear_sigm, null);
        mbb_labeler = new MBB_Labeler(null);
        camins_extrets = new ArrayList();
        profunditat = 0;
        // emplenam classes dels MBBs
        dades_conjunts_pathways = new MBB_Classifier();
        if (filename.endsWith(".dot")) {
            read_DAG_structure(filename);
        } else {
            System.out.println("PathExtraction: Invalid file name -/");
        }
    }

    ///////////////////////////////////////////////
    /*
     * GETTERS
     */
    public ArrayList<Path> getRetrievedPaths() {
        return camins_extrets;
    }

    public Label_Limiter getLabelInformation() {
        return label_info;
    }

    ///////////////////////////////////////////////
    /*
     * SETTERS
     */
    ///////////////////////////////////////////////
    /*
     * ALTRES MÈTODES AUXILIARS
     */
    ///////////////////////////////////////////////
    int cnt = 0;

    public void visita_node(Node node, String outcoming_dag_name) {
        // if (cnt%5000==0) System.out.println(cnt);

        cami_actual.add(node);
        profunditat++;
        if (node.getSuccessors().size() > 0) {
            // node interior, hi ha més profunditat
            node.getSuccessors().stream().map((i) -> nodes_DAG.get(i)).forEachOrdered((fill) -> {
                visita_node(fill, outcoming_dag_name);
            });
        } else {
            cnt++;
            // fulla
            if (profunditat < minimum_length) {
                // No afegim el camí, reduïnt en solucions poc representatives
            } else {
                // afegim el camí al conjunt calculat
                ArrayList<Node> cami_actual_copia = (ArrayList<Node>) cami_actual.clone();
                Path cami_extret = new Path(id_actual, outcoming_dag_name, cami_actual_copia);
                Path_Classifier pc = new Path_Classifier(cami_extret);
                cami_extret.setClasseCami(pc.getClasseCami());
                Path_Label pl = etiquetador_camins.getClassLabel(pc.getClasseCami());
                cami_extret.setEtiqCami(pl);
                if (pc.getClasseCami().getSigmesLiders().get(0) >= minimum_sigm) {
                    camins_extrets.add(cami_extret);
                    //
                    label_info.incrementLabel(pl);
                }
                id_actual++;
            }
        }
        // un pic completat el recorregut de la seva descendència, retiram el node
        // del camí parcialment calculat
        profunditat--;
        cami_actual.remove(node);
    }

    public void read_DAG_structure(String filename) throws IOException {
        // aquesta és la llista que s'anirà emplenant en llegir el contingut del fitxer
        index_actual = 0;
        nodes_DAG = new ArrayList(0);
        indexos_arrels = new ArrayList(0);
        edge_lines = new ArrayList();
        // es pretén escriure en un altre fitxer de resultats
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(filename));
            StringBuilder line_building = new StringBuilder(256);
            char read_char = (char) reader.read();
            while (read_char != '}') {
                line_building.delete(0, line_building.length());
                while (read_char != '}' && read_char != ';') {
                    line_building.append(read_char);
                    read_char = (char) reader.read();
                }
                // comprovam si estam llegint nodes del DAG o arestes
                if (read_char == '}') {
                    break;
                } else {
                    String line = line_building.toString();
                    try {
                        line = line.substring(line.indexOf("\""));
                        if (line.contains("->")) {
                            edge_lines.add(line);
                        } else {
                            process_node(line);
                        }
                        // read next line heading
                    } catch (Exception ex) {
                        //System.out.println("Line: " + line + " not readable");
                    }
                    read_char = (char) reader.read();
                }
            }
            reader.close();
        } catch (IOException e) {
        }
        mbb_labeler.dumpIntoFile();
        generateAllEdges();
    }

    public void process_node(String line) {
        String line_body = line.split("\t")[1];
        String[] dot_attributes = line_body.split(",");
        String node_id = dot_attributes[0].split("\"")[1].replace("n", "");
        String bio_id;
        boolean reaction = false;
        String enz_id = "";
        if (dot_attributes[1].contains("MBB")) {
            // es tracta d'un MBB
            bio_id = dot_attributes[1].split("\"")[1].replace("\\n", "").replace("MBB", "");
        } else {
            // es tracta d'una reacció
            reaction = true;
            String new_str = dot_attributes[1];
            int index = -1;
            while (new_str.equals(dot_attributes[1]) || new_str.contains("\\nR")) {
                index = new_str.indexOf("R");
                new_str = new_str.substring(index + 7);
                index = new_str.indexOf("\\n");
            }
            enz_id = new_str.substring(index + 2, new_str.indexOf("\""));

            bio_id = dot_attributes[1].substring(dot_attributes[1].indexOf("label=") + 7,
                    dot_attributes[1].indexOf("\\nR"));
        }
        ////
        Node nou_node = new Node(node_id, bio_id, reaction);
        nou_node.setClasse(dades_conjunts_pathways.getConjuntMBB(nou_node));
        nou_node.setLabel(mbb_labeler.getMBBLabel(bio_id));
        // Després ja l'eliminarem si veim que no constitueix una arrel
        nodes_DAG.add(nou_node);
        indexos_arrels.add(index_actual);

        index_actual++;
    }

    public void extract_DAG_paths(String filename) throws IOException {
        // ja tenint les nostres arrels guardades, organitzarem el recorregut en
        // profunditat partint d'aquestes
        camins_extrets = new ArrayList(0);
        id_actual = 0;
        indexos_arrels.stream().map((i) -> {
            cami_actual = new ArrayList(0);
            return i;
        }).map((i) -> nodes_DAG.get(i)).map((arrel) -> {
            profunditat = -1;
            return arrel;
        }).forEachOrdered((arrel) -> {
            String dag_name = filename.substring(filename.lastIndexOf("/") + 1);
            dag_name = dag_name.replace(".dot", "");
            visita_node(arrel, dag_name);
        });
        etiquetador_camins.dumpIntoFile();
    }

    private ArrayList<String> edge_lines;

    public void generateAllEdges() {
        for (String line : edge_lines) {
            process_edge(line);
        }
    }

    public void process_edge(String line) {
        // we first get the relation
        String relation = line.split("\"")[5];
        String[] parent_child = relation.split("x");
        String parent_id = parent_child[0].replace("e", "");
        String child_id = parent_child[1];
        Node parent_nd = new Node(parent_id, "");
        Node child_nd = new Node(child_id, "");
        int parent_index = nodes_DAG.indexOf(parent_nd);
        int child_index = nodes_DAG.indexOf(child_nd);

        if (indexos_arrels.contains(child_index)) {
            indexos_arrels.remove((Integer) child_index);
        }

        nodes_DAG.get(parent_index).addSuccessor(child_index);
    }
}

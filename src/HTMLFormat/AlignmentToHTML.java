/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HTMLFormat;

import QueryTool.querying.Query_Data;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/**
 *
 * @author ff
 */
public final class AlignmentToHTML extends Thread {

    private File alignment_file;

    private Query_Data qd;
    private HashSet<String> present_mbbls;

    private double[] column_scores;
    private String custom_styles;
    private String custom_script;

    private ArrayList<String> dag_path_names;
    private ArrayList<String[]> aligning_elements;

    private String html_file_name;

    @Override
    public void run() {
        try {
            retrieveAlignmentData();
            convertAlignmentToHTML();
        } catch (Exception ex) {

        }
    }

    public AlignmentToHTML(File alignment_file, Query_Data qd) throws IOException {
        this.alignment_file = alignment_file;
        this.qd = qd;
        this.html_file_name = this.alignment_file.getAbsolutePath().replace(".txt", ".html");
        present_mbbls = new HashSet();
        dag_path_names = new ArrayList();
        aligning_elements = new ArrayList();
    }

    private void retrieveAlignmentData() throws FileNotFoundException, IOException {
        FileReader fr = new FileReader(this.alignment_file);
        BufferedReader br = new BufferedReader(fr);
        String line = br.readLine();
        int cnt = 0;
        while (line != null && !line.equals("")) {
            if (cnt == 0) {
                // score matrix line
                String[] splitted = line.split("-->");
                String scores = splitted[1].replace(" ", "").replace("[", "").replace("]", "");
                String[] str_scores = scores.split(",");
                this.column_scores = new double[str_scores.length];
                for (int i = 0; i < str_scores.length; i++) {
                    this.column_scores[i] = Double.parseDouble(str_scores[i]);
                }
            } else {
                // mbbls
                String[] splitted = line.split("\\[");
                String dag_path_name = splitted[0].replace(" ", "");
                this.dag_path_names.add(dag_path_name);
                String mbbl_list = splitted[1].replace(" ", "").replace("]", "");
                String[] new_elems = mbbl_list.split(",");
                for (String elem : new_elems) {
                    this.present_mbbls.add(elem);
                }
                aligning_elements.add(new_elems);
            }
            cnt++;
            line = br.readLine();
        }
        computeMBBSimils();
    }

    private void computeMBBSimils() {
        custom_styles = "";
        custom_script = "$(document).ready(function() {\r\n";
        for (String mbbl_i : present_mbbls) {
            for (String mbbl_j : present_mbbls) {
                String mbbid_i = qd.getMBBId(mbbl_i);
                String mbbid_j = qd.getMBBId(mbbl_j);
                if (mbbid_i != null && mbbid_j != null) {
                    String style = "." + mbbl_i + "_" + mbbl_j + " {";
                    style += HTMLHelper.computeBGColorWithHue(qd.getMBBSimils(mbbid_i, mbbid_j));
                    style += "}\r\n";
                    custom_styles += style;

                    String script = "$(\"." + mbbl_i + "\").hover(function() {\n" + "    $(\"." + mbbl_j
                            + "\").addClass(\"" + mbbl_i + "_" + mbbl_j + "\");\n" + "}, function() {\n" + "    $(\"."
                            + mbbl_j + "\").removeClass(\"" + mbbl_i + "_" + mbbl_j + "\");\n" + "});\r\n";
                    custom_script += script;
                }
            }
        }
        custom_script += "\r\n});\r\n";
    }

    private void convertAlignmentToHTML() throws IOException {
        File html_file = new File(this.html_file_name);
        FileWriter fw = new FileWriter(html_file);
        BufferedWriter bw = new BufferedWriter(fw);
        initialHtmlCode(bw);
        alignmentTable(bw);
        mbbModals(bw);
        finalHtmlCode(bw);
        bw.close();
        fw.close();
    }

    private void initialHtmlCode(BufferedWriter bw) throws IOException {
        bw.write(HTMLHelper.openHTMLDoc());
        bw.write(HTMLHelper.HTMLHeadWithBootstrapAndJQ(this.custom_styles, this.custom_script));
        bw.write(HTMLHelper.openHTMLBody());
    }

    private void alignmentTable(BufferedWriter bw) throws IOException {
        bw.write(HTMLHelper.openResponsiveTable());
        bw.write(HTMLHelper.openTableHeader());
        bw.write(HTMLHelper.openTableRow());
        bw.write(HTMLHelper.tableHeadColumn(""));
        DecimalFormat dformatter = new DecimalFormat("#.###");
        for (double score : this.column_scores) {
            String st_score = dformatter.format(score);
            bw.write(HTMLHelper.tableHeadColumn(st_score));
        }
        bw.write(HTMLHelper.closeTableRow());
        bw.write(HTMLHelper.closeTableHeader());
        bw.write(HTMLHelper.openTableBody());
        for (int i = 0; i < this.dag_path_names.size(); i++) {
            bw.write(HTMLHelper.openTableRow());
            bw.write(HTMLHelper.tableColumn(this.dag_path_names.get(i)));
            for (int j = 0; j < this.aligning_elements.get(i).length; j++) {
                String elem = this.aligning_elements.get(i)[j];
                bw.write(HTMLHelper.tableColumn(elem, this.column_scores[j]));
            }
            bw.write(HTMLHelper.closeTableRow());
        }
        bw.write(HTMLHelper.closeTableBody());
        bw.write(HTMLHelper.closeResponsiveTable());
    }

    private void mbbModals(BufferedWriter bw) throws IOException {
        for (String mbbl : present_mbbls) {
            // mbb code
            if (!mbbl.contains("-")) {
                bw.write(HTMLHelper.openBootstrapModal(mbbl));
                bw.write(HTMLHelper.openTable());
                bw.write(HTMLHelper.openTableHeader());
                bw.write(HTMLHelper.openTableRow());
                bw.write(HTMLHelper.tableHeadColumn("MBBL Label"));
                bw.write(HTMLHelper.tableHeadColumn("MBB Full Code"));
                bw.write(HTMLHelper.closeTableRow());
                bw.write(HTMLHelper.closeTableHeader());
                bw.write(HTMLHelper.openTableBody());
                bw.write(HTMLHelper.openTableRow());
                bw.write(HTMLHelper.tableColumn(mbbl));
                bw.write(HTMLHelper.tableColumn(qd.getMBBId(mbbl)));
                bw.write(HTMLHelper.closeTableRow());
                bw.write(HTMLHelper.closeTableBody());
                bw.write(HTMLHelper.closeTable());

                bw.write(HTMLHelper.openList());
                String mbbid = qd.getMBBId(mbbl);
                ArrayList<String> reactions = qd.getMBBReactions(mbbid);
                // System.out.println(mbbl + " - " + mbbid);
                // System.out.println(reactions);
                try {
                    for (String reaction : reactions) {
                        bw.write(HTMLHelper.redirectingListItem(reaction, qd.reactionLink(reaction)));
                    }
                } catch (Exception ex) {
                }
                bw.write(HTMLHelper.closeList());

                bw.write(HTMLHelper.closeBootstrapModal());
            }
        }
    }

    private void finalHtmlCode(BufferedWriter bw) throws IOException {
        bw.write(HTMLHelper.closeHTMLBody());
        bw.write(HTMLHelper.closeHTMLDoc());
    }
}

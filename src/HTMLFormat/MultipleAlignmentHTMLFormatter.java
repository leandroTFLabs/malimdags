/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HTMLFormat;

import QueryTool.querying.Query_Data;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author ff
 */
public final class MultipleAlignmentHTMLFormatter {

    private final Query_Data qd;

    public MultipleAlignmentHTMLFormatter(Query_Data qd) throws IOException {
        this.qd = qd;
        processAlignmentsHTML();
    }

    public void processAlignmentsHTML() throws IOException {
        ArrayList<AlignmentToHTML> await_for_files = new ArrayList();
        String root_dir = System.getProperty("user.dir");
        String ma_dirname = root_dir + "/DATA/calculated/MULTIPLE-ALIGNMENTS/";
        boolean mkdirIFNEX = new File(ma_dirname).mkdir();
        if (mkdirIFNEX) {
            // System.out.println("No multiple alignments");
        } else {
            File f = new File(ma_dirname);
            for (File set_dir : f.listFiles()) {
                if (set_dir.isDirectory()) {
                    for (File alignment_file : set_dir.listFiles()) {
                        if (!alignment_file.isDirectory() && alignment_file.getName().contains(".txt")) {
                            processAlignmentFileHTML(alignment_file,await_for_files);
                        }
                    }
                }
            }
        }
        for (AlignmentToHTML t : await_for_files) {
            try {
                t.join();
            } catch (InterruptedException ex) {

            }
        }
    }

    public void processAlignmentFileHTML(File text_file, ArrayList<AlignmentToHTML> threads_list) throws IOException {
        AlignmentToHTML align_file = new AlignmentToHTML(text_file, qd);
        align_file.start();
        threads_list.add(align_file);
    }
}

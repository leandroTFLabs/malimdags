/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HTMLFormat;

/**
 *
 * @author ff
 */
public class HTMLHelper {

    public static final double THRESH_HUE = 0.8;
    public static final String HIGH_HUE_TEXT_COLOR = "#ffff";

    //224, 90, 204
    public static final int PREDEF_BG_COLOR_R = 224;
    public static final int PREDEF_BG_COLOR_G = 90;
    public static final int PREDEF_BG_COLOR_B = 204;
    
    //90, 90, 204
    public static final int PREDEF_BG_COLOR_R_OOT = 90;
    public static final int PREDEF_BG_COLOR_G_OOT = 90;
    public static final int PREDEF_BG_COLOR_B_OOT = 204;

    public static final String HEADER_CONTENT
            = "<!--BOOTSTRAP Y JQUERY-->\n"
            + "<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">\n"
            + "<script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>\n"
            + "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\" integrity=\"sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1\" crossorigin=\"anonymous\"></script>\n"
            + "<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\" integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\" crossorigin=\"anonymous\"></script>\n"
            + "<!--FONT AWESOME ICONS-->\n"
            + "<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.7.0/css/all.css\" integrity=\"sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ\" crossorigin=\"anonymous\">\n"
            + "<!--DATATABLES-->\n"
            + "<link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdn.datatables.net/v/dt/dt-1.10.24/datatables.min.css\"/>\n"
            + "<script type=\"text/javascript\" src=\"https://cdn.datatables.net/v/dt/dt-1.10.24/datatables.min.js\"></script>\n"
            + "<script type=\"text/javascript\" src=\"https://cdn.datatables.net/fixedcolumns/3.2.1/js/dataTables.fixedColumns.min.js\"></script>"
            + "<!--EVENTOS JQUERY PRINCIPALES-->\n"
            + "<style>\r\n"
            + ".illuminate:hover {\n" +
            "    color: #f1f1f1;\n" +
            "    text-shadow: 0 0 1px #fff, 0 0 2px #fff, 0 0 3px #e6bc00, 0 0 4px #e6bc00, 0 0 5px #e6bc00, 0 0 6px #e6bc00, 0 0 7px #e6bc00;\n" +
            "    transition: 0.1s;\n" +
            "}"
            + ".bg_predef {\n" +
            "    padding: 10px;\n" +
            "    background-color: rgba("+PREDEF_BG_COLOR_R_OOT+","+PREDEF_BG_COLOR_G_OOT+","+PREDEF_BG_COLOR_B_OOT+",1.0);\n" +
            "    color: white;\n" +
            "}"
            + ".fade {\n" +
            "    opacity: 0;\n" +
            "    -webkit-transition: opacity 1s linear;\n" +
            "    transition: opacity 1s linear;\n" +
            "}"
            + "\r\n</style>\r\n"
            + "<script>\n"
            + "  $(document).ready(function() {\n"
            + "  $('#datatable').DataTable( {\n" +
            "        scrollY:        \"80vh\",\n" +
            "        scrollX:        true,\n" +
            "        scrollCollapse: true,\n" +
            "        paging:         false,\n" +
            "        fixedColumns:   {\n" +
            "            leftColumns: 1,\n" +
            "            rightColumns: 0\n" +
            "        }\n" +
            "    } )});\n"
            + "  function openModal(modal_id) {\n"
            + "    $(\"#\"+modal_id).modal(\"show\");\n"
            + "  }\n"
            + "</script>";
    
    public static String computeBGColorWithHue(double hue) {
        String text_color = hue > THRESH_HUE ? "white" : "black";
        return "\r\nbackground-color: rgba(" + PREDEF_BG_COLOR_R_OOT + "," + PREDEF_BG_COLOR_G_OOT + "," + PREDEF_BG_COLOR_B_OOT + "," + hue + ") !important;\r\n"
                + "color: " + text_color + " !important;\r\n";
    }

    public static String openHTMLDoc() {
        return "<html>\r\n";
    }

    public static String closeHTMLDoc() {
        return "</html>";
    }

    public static String HTMLHeadWithBootstrapAndJQ(String custom_styles, String custom_scripting) {
        return "<head>\r\n" + HEADER_CONTENT + "\r\n"
                + "<style\r\n>" + custom_styles + "</style>\r\n"
                + "<script\r\n>" + custom_scripting + "</script>" + "\r\n</head>\r\n";
    }

    public static String openHTMLBody() {
        return "<body class=\"bg-dark\">\r\n<div role=\"main\">\r\n";
    }

    public static String closeHTMLBody() {
        return "</div>\r\n</body>\r\n";
    }
    
    public static String openTable() {
        return "<div class=\"table-responsive\">\r\n<table class=\"display table\">\r\n";
    }
    
    public static String closeTable() {
        return "</table>\r\n</div>\r\n";
    }
    
    public static String openList() {
        return "<ul style=\"max-height:300px; overflow-y: auto; overflow-x: hidden;\" class=\"w-100 rounded bg_predef\">\r\n";
    }
    
    public static String closeList() {
        return "</ul>\r\n";
    }
    
    public static String redirectingListItem(String name, String link) {
        return "<li>\r\n<span class=\"illuminate\" style=\"cursor: pointer\" onclick=\"window.open(\'"+link+"\',\'"+name+"\').focus();\">"+name+"</span>\r\n</li>\r\n";
    }

    public static String openResponsiveTable() {
        return "<div class=\"cardbox m-2 p-3 bg-white rounded\"><table id=\"datatable\" class=\"stripe row-border order-column\" style=\"width:100%\">\r\n";
    }

    public static String closeResponsiveTable() {
        return "</table>\r\n</div>\r\n";
    }

    public static String openTableHeader() {
        return "<thead>\r\n";
    }

    public static String closeTableHeader() {
        return "</thead>\r\n";
    }

    public static String openTableBody() {
        return "<tbody>\r\n";
    }

    public static String closeTableBody() {
        return "</tbody>\r\n";
    }

    public static String openTableRow() {
        return "<tr>\r\n";
    }

    public static String closeTableRow() {
        return "</tr>\r\n";
    }

    public static String tableHeadColumn(String content) {
        return "<th>" + content + "</th>\r\n";
    }

    public static String tableColumn(String content) {
        return "<td class=\"bg-dark text-white\">" + content + "</td>\r\n";
    }

    public static String tableColumn(String content, double a) {
        String text_color = a > THRESH_HUE ? " color: " + HIGH_HUE_TEXT_COLOR + ";" : "";
        String elem_style = content.equals("-") ? "" : " cursor: pointer; background-color: rgba(" + PREDEF_BG_COLOR_G + "," + PREDEF_BG_COLOR_G + "," + PREDEF_BG_COLOR_B + "," + a + "); "+text_color;
        String elem_class = content.equals("-") ? "" : content+" illuminate";
        return "<td class=\"text-center "+elem_class+"\" style=\""+elem_style+"\""
                + " onclick=\" openModal(\'modal_" + content + "\')\">" + content + "</td>\r\n";
    }

    public static String openBootstrapModal(String modal_id) {
        return "<div id=\"modal_" + modal_id + "\" class=\"modal\" tabindex=\"-1\" role=\"dialog\">\n"
                + "    <div class=\"modal-dialog\" role=\"document\">\n"
                + "        <div class=\"modal-content\">\n"
                + "            <div class=\"modal-header\">\n"
                + "                <h5 class=\"modal-title\">" + modal_id + "</h5>\n"
                + "                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n"
                + "                    <span aria-hidden=\"true\">&times;</span>\n"
                + "                </button>\n"
                + "            </div>\n"
                + "            <div class=\"modal-body\">";
    }

    public static String closeBootstrapModal() {
        return "            </div>\n"
                + "            <div class=\"modal-footer\">\n"
                + "                <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n"
                + "            </div>\n"
                + "        </div>\n"
                + "    </div>\n"
                + "</div>";
    }
}

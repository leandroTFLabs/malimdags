/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Forest;

import Auxiliar.Clustering.Path_Class;
import Auxiliar.Clustering.Path_Label;
import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public interface Paths_Interface {
    public int getIdCami();
    public String getNomDag();
    public ArrayList<Node> getNodesCami();
    public int getPathSize();
    public Path_Class getClasseCami();
    public Path_Label getEtiqCami();
    public boolean overMinimSigm(double sigma);
    public void setClasseCami(Path_Class classe_cami);
    public void setEtiqCami(Path_Label etiq_cami);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Forest;

import Auxiliar.Clustering.Label_Limiter;
import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public interface DAGs_Interface {
    public String getDagName();
    public ArrayList<Path> getDagPaths();
    public Label_Limiter getLabelInformation();
    public void addPath(Path cami);
    public void makeupStats();
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Forest;

import Auxiliar.Comparison.Compare_Paths;
import Auxiliar.Clustering.Label_Limiter;
import Disk_Mgmt.DAG_Paths_Dumper;
import Disk_Mgmt.DAG_Paths_Extractor;
import Stats.DAG_Stats;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class DAG implements DAGs_Interface {
    
    /*
    ATRIBUTS
    */
    private String nom_DAG;
    private ArrayList<Path> camins_DAG;
    private DAG_Stats stats;
    private Label_Limiter label_info;
    ///////////////////////////////////////////////
    /*
    CONSTRUCTORS
    */
    public DAG(String nom_DAG) {
        this.stats = new DAG_Stats(this);
        this.nom_DAG = nom_DAG;
        this.camins_DAG = new ArrayList();
    }
    public DAG(String nom_DAG, ArrayList<Path> camins_DAG) {
        this.stats = new DAG_Stats(this);
        this.nom_DAG = nom_DAG;
        this.camins_DAG = camins_DAG;
    }
    ///////////////////////////////////////////////
    /*
    GETTERS
    */
    @Override
    public String getDagName() {
        return nom_DAG;
    }
    @Override
    public ArrayList<Path> getDagPaths() {
        return camins_DAG;
    }
    @Override
    public Label_Limiter getLabelInformation() {
        return label_info;
    }
    ///////////////////////////////////////////////
    /*
    SETTERS
    */
    @Override
    public void addPath(Path cami) {
        this.camins_DAG.add(cami);
    }
    @Override
    public void makeupStats() {
        this.stats.calculaStats();
    }
    ///////////////////////////////////////////////
    /*
    ALTRES MÈTODES AUXILIARS
    */
    ///////////////////////////////////////////////
    public void extractFromFile(boolean bear_sigm, double minimum_sigm, int minimum_length) throws IOException {
        String currentDir = System.getProperty("user.dir");
        String pathwysDir = currentDir.concat("/DATA/DAGs_to_process/");
        String dag_file_Dir = pathwysDir.concat(nom_DAG+".dot");
        DAG_Paths_Extractor pe = new DAG_Paths_Extractor(dag_file_Dir, bear_sigm, minimum_sigm, minimum_length);
        pe.read_DAG_structure(dag_file_Dir);
        pe.extract_DAG_paths(dag_file_Dir);
        camins_DAG = pe.getRetrievedPaths();
        camins_DAG.sort(new Compare_Paths());
        label_info = pe.getLabelInformation();
    }
    
    public void dumpIntoFile() throws IOException {
        makeupStats();
        DAG_Paths_Dumper pd = new DAG_Paths_Dumper(this);
        pd.dump_DAG_paths();
    }
    
    @Override
    public String toString() {
        String descripcio_general = "S'han extret un total de " + camins_DAG.size() + " camins\r\n";
        String stats = this.stats.toString();
        String descripcio_etiquetes = "*Informació referent a etiquetes\r\n";
        String info_etiquetes = label_info.toString();
        return
            descripcio_general +
            stats +
            descripcio_etiquetes +
            info_etiquetes;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Forest;

import Auxiliar.Clustering.Path_Class;
import Auxiliar.Clustering.Path_Label;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author ASUS
 */
public class Path implements Paths_Interface {
    /*
    ATRIBUTS
    */
    private int id;
    private String nom_DAG;
    private ArrayList<Node> nodes_cami;
    private Path_Class classe_cami;
    private Path_Label etiq_cami;
    ///////////////////////////////////////////////
    /*
    CONSTRUCTORS
    */
    public Path(int id, String nom_DAG, ArrayList<Node> nodes_cami) {
        this.id = id;
        this.nom_DAG = nom_DAG;
        this.nodes_cami = nodes_cami;
    }
    ///////////////////////////////////////////////
    /*
    GETTERS
    */
    @Override
    public int getIdCami() {
        return id;
    }
    @Override
    public String getNomDag() {
        return nom_DAG;
    }
    @Override
    public ArrayList<Node> getNodesCami() {
        return nodes_cami;
    }
    @Override
    public int getPathSize() {
        return nodes_cami.size();
    }
    @Override
    public Path_Class getClasseCami() {
        return classe_cami;
    }
    @Override
    public Path_Label getEtiqCami() {
        return etiq_cami;
    }
    @Override
    public boolean overMinimSigm(double sigma) {
        return !etiq_cami.getBearSigm() || classe_cami.getSigmesLiders().get(0) >= sigma;
    }
    ///////////////////////////////////////////////
    /*
    SETTERS
    */
    @Override
    public void setClasseCami(Path_Class classe_cami) {
        this.classe_cami = classe_cami;
    }
    @Override
    public void setEtiqCami(Path_Label etiq_cami) {
        this.etiq_cami = etiq_cami;
    }
    ///////////////////////////////////////////////
    /*
    ALTRES MÈTODES AUXILIARS
    */
    ///////////////////////////////////////////////
    @Override
    public String toString() {
        String to_return = "";
        to_return = this.nom_DAG+"-"+this.id;
        return to_return;
    }
    public String getDescriptor() {
        return "p"+id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + this.id;
        hash = 17 * hash + Objects.hashCode(this.nom_DAG);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Path other = (Path) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.nom_DAG, other.nom_DAG)) {
            return false;
        }
        return true;
    }
    
    public boolean isNoPath() {
        return id == -1;
    }
    public static Path noPath() {
        return new Path(-1,"No_Path",new ArrayList());
    }
}

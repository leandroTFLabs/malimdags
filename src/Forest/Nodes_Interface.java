/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Forest;

import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public interface Nodes_Interface {
    public String getId();
    public ArrayList<Integer> getSuccessors();
    public String getBioId();
    public String getLabel();
    public ArrayList<String> getClasse();
    public boolean isReaction();
    public void addSuccessor(int index_successor);
    public void setLabel(String label);
    public void setClasse(ArrayList<String> classe);
}

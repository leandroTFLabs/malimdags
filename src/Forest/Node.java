/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Forest;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author ASUS
 */
public class Node implements Nodes_Interface {
    /*
    ATRIBUTS
    */
    private ArrayList<Integer> successors;
    private String id;
    private String label;
    private String bio_id;
    private ArrayList<String> classe;
    private boolean is_reaction;
    ///////////////////////////////////////////////
    /*
    CONSTRUCTORS
    */
    public Node(String id, String bio_id, ArrayList<String> classe, boolean is_reaction) {
        this.successors = new ArrayList();
        this.id = id;
        this.bio_id = bio_id;
        this.classe = classe;
        this.is_reaction = is_reaction;
    }
    public Node(String id, String bio_id, boolean is_reaction) {
        this.successors = new ArrayList();
        this.id = id;
        this.bio_id = bio_id;
        this.classe = new ArrayList();
        this.is_reaction = is_reaction;
    }
    public Node(String id, String bio_id) {
        this.successors = new ArrayList();
        this.id = id;
        this.bio_id = bio_id;
        this.classe = new ArrayList();
        this.is_reaction = false;
    }
    public Node(String label) {
        this.successors = new ArrayList();
        this.id = "unnecessary";
        this.bio_id = "unnecessary";
        this.classe = new ArrayList();
        this.is_reaction = false;
        this.label = label;
    }
    ///////////////////////////////////////////////
    /*
    GETTERS
    */
    @Override
    public String getId() {
        return id;
    }
    @Override
    public ArrayList<Integer> getSuccessors() {
        return successors;
    }
    @Override
    public String getBioId() {
        return bio_id;
    }
    @Override
    public String getLabel() {
        return label;
    }
    @Override
    public ArrayList<String> getClasse() {
        return classe;
    }
    @Override
    public boolean isReaction() {
        return is_reaction;
    }
    ///////////////////////////////////////////////
    /*
    SETTERS
    */
    @Override
    public void addSuccessor(int index_successor) {
        successors.add(index_successor);
    }
    @Override
    public void setLabel(String label) {
        this.label = label;
    }
    @Override
    public void setClasse(ArrayList<String> classe) {
        this.classe = classe;
    }
    ///////////////////////////////////////////////
    /*
    ALTRES MÈTODES AUXILIARS
    */
    ///////////////////////////////////////////////

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Node other = (Node) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return label;
    }
    
}

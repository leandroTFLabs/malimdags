/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.PathAlignments.PairWise;

import Auxiliar.Comparison.MBB_Simil_Data;
import Forest.Node;
import Forest.Path;

/**
 *
 * @author ASUS
 */
public class Path_Aligner {
    /*
    ATRIBUTS
    */
    private Path p1, p2;
    private MBB_Simil_Data mbb_simils;
    private PW_Dynamic_Sequence_Aligner aligner;
    private double[][] simils_matrix;
    private final double GAP_PENALTY = 0.5;
    ///////////////////////////////////////////////
    /*
    CONSTRUCTORS
    */
    public Path_Aligner(Path p1, Path p2, MBB_Simil_Data mbb_simils) {
        if (p2.getPathSize() > p1.getPathSize()) {
            this.p1 = p2;
            this.p2 = p1;
        } else {
            this.p1 = p1;
            this.p2 = p2;
        }
        this.mbb_simils = mbb_simils;
        this.simils_matrix = new double[this.p1.getPathSize()][this.p2.getPathSize()];
        initSimilMatrix();
        proceedToAlign();
    }
    ///////////////////////////////////////////////
    /*
    GETTERS
    */
    public double getBestCompatibility() {
        return aligner.getMeanSimil();
    }
    ///////////////////////////////////////////////
    /*
    SETTERS
    */
    ///////////////////////////////////////////////
    /*
    ALTRES MÈTODES AUXILIARS
    */
    private void initSimilMatrix() {
        int i = 0;
        for (Node ni : p1.getNodesCami()) {
            int j = 0;
            for (Node nj : p2.getNodesCami()) {
                this.simils_matrix[i][j] = this.mbb_simils.getSimilMBBs(ni.getLabel(), nj.getLabel());
                j++;
            }
            i++;
        }
    }
    private void proceedToAlign() {
        this.aligner = new PW_Dynamic_Sequence_Aligner(this.simils_matrix,GAP_PENALTY);
    }
    ///////////////////////////////////////////////
}

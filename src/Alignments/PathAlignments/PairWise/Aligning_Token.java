/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.PathAlignments.PairWise;

/**
 *
 * @author ASUS
 */
public class Aligning_Token {
    private boolean gap;
    public Aligning_Token(boolean gap) {
        this.gap = gap;
    }
    public boolean getGap() {
        return gap;
    }
    @Override
    public String toString() {
        if (this.gap) return "gap";
        return "elt";
    }
}

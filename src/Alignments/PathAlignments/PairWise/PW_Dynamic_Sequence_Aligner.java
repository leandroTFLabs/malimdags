/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.PathAlignments.PairWise;

import Alignments.PathAlignments.PairWise.Aligning_Token;
import Auxiliar.Utils;
import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class PW_Dynamic_Sequence_Aligner {
    //alignment tools
    private double[][] similsMatrix;
    private double gapPenalty;
    private int n,m;
    private double[][] substitutionMatrix;

    //alignment results
    private ArrayList<Aligning_Token> aligned_1;
    private ArrayList<Aligning_Token> aligned_2;
    private double accumSimil = 0.0;

    public PW_Dynamic_Sequence_Aligner(double[][] similsMatrix, double gapPenalty) {
        this.similsMatrix = similsMatrix;
        this.gapPenalty = gapPenalty;
        this.n = this.similsMatrix.length;
        this.m = this.similsMatrix[0].length;
        this.aligned_1 = new ArrayList();
        this.aligned_2 = new ArrayList();
        calcOptimalAlignment();
    }
    
    public PW_Dynamic_Sequence_Aligner(double[][] similsMatrix) {
        new PW_Dynamic_Sequence_Aligner(similsMatrix,0.5);
    }

    private void calcOptimalAlignment() {
            //Initialize 2D arrays for memoization
            substitutionMatrix = new double[n+1][m+1];

            //Array bounds are < seq1.length() (not <= ) since both sequences have a blank space @ the start
            //Fill 0th column
            for (int i = 0; i < n+1; i++) {	// base case: j = 0
                substitutionMatrix[i][0] = i*gapPenalty;
            }
            //Fill 0th row
            for (int j = 0; j < m+1; j++) {	// base case: i = 0
                substitutionMatrix[0][j] = j*gapPenalty;
            }

            //Fill rest of memo table
            for (int j = 1; j < m+1; j++) {
                for (int i = 1; i < n+1; i++) {
                    double alignedCharWithCharPenalty = mismatchPenalty(i, j) + substitutionMatrix[i - 1][j - 1];	//case1: seq1[i] & seq2[j] aligned with each other
                    double seq1CharWithGap = gapPenalty + substitutionMatrix[i - 1][j];		//case2: seq1 with gap
                    double seq2CharWithGap = gapPenalty + substitutionMatrix[i][j - 1];		//case3: seq2 with gap
                    //Calculate the min of 3 values & store predecessors
                    if (alignedCharWithCharPenalty <= seq1CharWithGap && alignedCharWithCharPenalty <= seq2CharWithGap) {			//case1 is the min
                        substitutionMatrix[i][j] = alignedCharWithCharPenalty;
                    }
                    else if (seq1CharWithGap <= alignedCharWithCharPenalty && seq1CharWithGap <= seq2CharWithGap) {	//case2 is the min
                        substitutionMatrix[i][j] = seq1CharWithGap;
                    }
                    else {	//case3 is the min
                        substitutionMatrix[i][j] = seq2CharWithGap;
                    }
                }
            }

            findAlignment();
    }

    //Retrace the memoTable to find the actual alignment, not just the minimum cost
    private void findAlignment() {
        int i = n; //-1 since seq1 & seq2 have leading space
        int j = m;

        //Retrace the memoTable calculations. Stops when reaches the start of 1 sequence (so additional gaps may still need to be added to the other)
        while (i > 0 && j > 0) {
            if (substitutionMatrix[i][j] - mismatchPenalty(i, j) == substitutionMatrix[i - 1][j - 1]) { //case1: both aligned
                addGapOrNot(false,false);
                accumSimil += (1 - mismatchPenalty(i,j));
                i--;
                j--;
            }
            else if (substitutionMatrix[i][j] - gapPenalty == substitutionMatrix[i - 1][j]) { //case2: seq2 with gap
                addGapOrNot(false,true);
                i--;
            }
            else if (substitutionMatrix[i][j] - gapPenalty == substitutionMatrix[i][j - 1]) { //case3: seq1 with gap
                addGapOrNot(true,false);
                j--;
            }
        }
        while (i > 0) {		
            addGapOrNot(false,true);
            i--;
        }
        while (j > 0) {		
            addGapOrNot(true,false);
            j--;
        }
        aligned_1 = Utils.revertArrayList(aligned_1);
        aligned_2 = Utils.revertArrayList(aligned_2);
    }

    private double mismatchPenalty(int i, int j) {
        return 1 - similsMatrix[i-1][j-1];
    }

    private void addGapOrNot(boolean seq1gap, boolean seq2gap) {
        this.aligned_1.add(new Aligning_Token(seq1gap));
        this.aligned_2.add(new Aligning_Token(seq2gap));
    }

    public double getMeanSimil() {
        return accumSimil/Math.max(n, m);
    }
    
    public int getN() {
        return n;
    }
    
    public int getM() {
        return m;
    }

    public ArrayList<Aligning_Token>[] getAlignedSequences() {
        ArrayList<Aligning_Token>[] sequences = new ArrayList[2];
        sequences[0] = aligned_1;
        sequences[1] = aligned_2;
        return sequences;
    }
}

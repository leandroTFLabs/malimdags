/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.PathAlignments.Multiple.PITools;

import Alignments.PathAlignments.PairWise.PW_Dynamic_Sequence_Aligner;
import java.util.Objects;

/**
 *
 * @author ASUS
 */
public class Pair_Aligned {
    
    private Pair pair;
    private PW_Dynamic_Sequence_Aligner align;
    
    public Pair_Aligned(Pair pair, PW_Dynamic_Sequence_Aligner align) {
        this.pair = pair;
        this.align = align;
    }
    
    public Pair getPair() {
        return pair;
    }
    
    public PW_Dynamic_Sequence_Aligner getAlign() {
        return align;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.pair);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pair_Aligned other = (Pair_Aligned) obj;
        if (!Objects.equals(this.pair, other.pair)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "pair: "+pair.getElems()[0]+"\t-\t"+pair.getElems()[1];
    }
    
}

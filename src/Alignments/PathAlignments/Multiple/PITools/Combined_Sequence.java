/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.PathAlignments.Multiple.PITools;

import Alignments.PathAlignments.PairWise.Aligning_Token;
import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author ASUS
 */
public class Combined_Sequence {
    private ArrayList<Aligning_Token> tokens;
    private HashSet<Integer> combined_indexes;
    public Combined_Sequence(ArrayList<Aligning_Token> tokens, HashSet<Integer> combined_indexes) {
        this.tokens = tokens;
        this.combined_indexes = combined_indexes;
    }
    public ArrayList<Aligning_Token> getTokens() {
        return tokens;
    }
    public HashSet<Integer> getCombined() {
        return combined_indexes;
    }
    public boolean combinedIndex(int index) {
        return combined_indexes.contains(index);
    }
    public void registerIndex(int index) {
        combined_indexes.add(index);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.PathAlignments.Multiple.PITools;

import Auxiliar.Comparison.Compare_Aligned_Pairs;
import Alignments.PathAlignments.PairWise.PW_Dynamic_Sequence_Aligner;
import Forest.Path;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author ASUS
 */
public class MT_Progressive {
    
    private GreedySorting greedy;
    private ArrayList<Paired_Module> pairs;
    private HashMap<Pair,PW_Dynamic_Sequence_Aligner> align_map;
    private ArrayList<Pair_Aligned> align_list;
    private Multiple_Alignment<Path> final_aligned;
    
    public MT_Progressive(ArrayList<Paired_Module> pairs) {
        this.greedy = new GreedySorting();
        this.pairs = pairs;
        this.align_map = new HashMap();
        this.align_list = new ArrayList();
        proceedToAlign();
    }
    
    public Multiple_Alignment<Path> getFinalAlignment() {
        return final_aligned;
    }

    private void proceedToAlign() {
        calculatePairAlignments();
        buildGreedyStructure();
        composeGreedyStructure();
    }

    private void calculatePairAlignments() {
        pairs.forEach(pm -> {
            PW_Dynamic_Sequence_Aligner pdsa = new PW_Dynamic_Sequence_Aligner(pm.getSimilsMatrix(),0.5);
            Pair_Aligned pa = new Pair_Aligned(pm.getPair(),pdsa);
            align_map.put(pm.getPair(),pdsa);
            align_list.add(pa);
        });
    }

    private void buildGreedyStructure() {
        align_list.sort(new Compare_Aligned_Pairs());
        while (!align_list.isEmpty()) {
            Pair_Aligned pa = align_list.get(align_list.size() - 1);
            if (greedy.nodesPairIsPossible(pa.getPair())) {
                greedy.addNodesPair(pa.getPair());
            }
            align_list.remove(align_list.size() - 1);
        }
    }

    private void composeGreedyStructure() {
        greedy.calcFinalAlignment(align_map);
        this.final_aligned = greedy.getFinalAlignment();
    }
}

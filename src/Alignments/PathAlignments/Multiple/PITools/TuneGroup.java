/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.PathAlignments.Multiple.PITools;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Objects;

class SubSetsTNod {

    private ArrayList<SubSetsTNod> children;
    private ArrayList<Position> this_level;
    private int fewer_level;
    private boolean children_calculated = false;
    private boolean expelled = false;
    private SubSetsTree invoker;

    public SubSetsTNod(ArrayList<Position> this_level, SubSetsTree invoker) {
        this.this_level = this_level;
        this.fewer_level = this.this_level.size() - 1;
        this.children = new ArrayList<>();
        this.invoker = invoker;
    }

    public void calculateChildren() {
//        if (!this.invoker.isKnownChild(this)) {
//            this.invoker.markAsKnown(this);
//        }
//        for (int o = this.this_level.size() - this.fewer_level; o >= 0; o--) {
//            ArrayList<Position> new_sub_set = new ArrayList();
//            for (int i_p = o; i_p < this.fewer_level; i_p++) {
//                new_sub_set.add(this.this_level.get(i_p));
//            }
//            if (new_sub_set.size() > 0) {
//                SubSetsTNod this_child = new SubSetsTNod(new_sub_set, invoker);
//                if (!this.invoker.isKnownChild(this_child)) {
//                    this.children.add(this_child);
//                    this.invoker.markAsKnown(this_child);
//                }
//            }
//        }

        this.children_calculated = true;
    }

    public ArrayList<SubSetsTNod> getChildren() {
        return this.children;
    }

    public ArrayList<Position> getThisLevel() {
        return this.this_level;
    }

    public boolean getChildrenCalculated() {
        return this.children_calculated;
    }

    public boolean isExpelled() {
        return this.expelled;
    }

    public void markChildrenAsExpelled() {
        this.children.forEach(child -> {
            child.markAsExpelled();
        });
    }

    public void markAsExpelled() {
        this.expelled = true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.this_level);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SubSetsTNod other = (SubSetsTNod) obj;
        if (!Objects.equals(this.this_level, other.this_level)) {
            return false;
        }
        return true;
    }
}

class SubSetsTree {

    public static final int MAX_STACK_SIZE = 100000;

    private HashSet<SubSetsTNod> known_children;

    private SubSetsTNod root;
    private SubSetsTNod current_visit;
    private ArrayList<SubSetsTNod> traversal_stack;

    public SubSetsTree(ArrayList<Position> main_set) {
        this.root = new SubSetsTNod(main_set, this);
        this.known_children = new HashSet();
    }
    
    public SubSetsTree(ArrayList<Position> main_set, HashSet known_children) {
        this.root = new SubSetsTNod(main_set, this);
        this.known_children = known_children;
    }

    public void prepareForTraversal() {
        this.current_visit = root;
        this.traversal_stack = new ArrayList();
        this.traversal_stack.add(root);
    }

    public ArrayList<Position> getNextSubSet() {
        for (int i = this.traversal_stack.size() - 1; i >= 0; i--) {
            if (this.traversal_stack.get(i).isExpelled() || this.isKnownChild(this.traversal_stack.get(i))) this.traversal_stack.remove(i);
        }
        if (this.traversal_stack.isEmpty()) {
            return null;
        }
        this.current_visit = this.traversal_stack.get(0);
        this.traversal_stack.remove(0);
        if (!current_visit.getChildrenCalculated()) {
            current_visit.calculateChildren();
            if (this.traversal_stack.size() <= MAX_STACK_SIZE) {
                current_visit.getChildren().forEach(node -> {
                    //System.out.println("tHERE A CHILD");
                    //System.out.println("Hah");
                    this.traversal_stack.add(node);
                });
            }
        }
        return this.current_visit.getThisLevel();
    }

    public void markCurrChildrenAsExpelled() {
        this.current_visit.markChildrenAsExpelled();
    }

    public boolean isKnownChild(SubSetsTNod child) {
        return this.known_children.contains(child);
    }

    public void markAsKnown(SubSetsTNod child) {
        this.known_children.add(child);
    }
    
    public HashSet<SubSetsTNod> getKnownChildren() {
        return this.known_children;
    }
}

class TuneGroupComparator implements Comparator<TuneGroup> {

    @Override
    public int compare(TuneGroup o1, TuneGroup o2) {
        return Integer.compare(o2.getPositions().size(),o1.getPositions().size());
    }
    
}

/**
 *
 * @author ff
 */
public class TuneGroup {

    private ArrayList<Position> positions;
    private SubSetsTree subsets_tree;

    public TuneGroup() {
        this.positions = new ArrayList();
    }

    public void addPosition(Position p) {
        this.positions.add(p);
    }

    public ArrayList<Position> getPositions() {
        return this.positions;
    }

    public void initSubSets() {
        this.subsets_tree = new SubSetsTree(this.positions);
        this.subsets_tree.prepareForTraversal();
    }
    
    public void updateSubSets() {
        this.subsets_tree = new SubSetsTree(this.positions,this.subsets_tree.getKnownChildren());
        this.subsets_tree.prepareForTraversal();
    }

    public SubSetsTree getSSTree() {
        return this.subsets_tree;
    }
    
    @Override
    public String toString() {
        return this.positions.toString();
    }

}

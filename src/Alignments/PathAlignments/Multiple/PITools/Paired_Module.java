/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.PathAlignments.Multiple.PITools;

import java.util.Objects;

/**
 *
 * @author ASUS
 */
public class Paired_Module {
    private Pair pair;
    private double[][] similsMatrix;
    public Paired_Module(Pair pair, double[][] similsMatrix) {
        this.pair = pair;
        this.similsMatrix = similsMatrix;
    }
    public Pair getPair() {
        return pair;
    }
    public double[][] getSimilsMatrix() {
        return similsMatrix;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.pair);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Paired_Module other = (Paired_Module) obj;
        if (!Objects.equals(this.pair, other.pair)) {
            return false;
        }
        return true;
    }

    
    
    @Override
    public String toString() {
        return "pair: "+pair.getElems()[0]+"\t-\t"+pair.getElems()[1];
    }
}

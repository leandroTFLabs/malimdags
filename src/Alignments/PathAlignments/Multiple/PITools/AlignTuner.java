/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.PathAlignments.Multiple.PITools;

import static Alignments.PathAlignments.Multiple.PITools.Multiple_Alignment.MOVABLE_L;
import static Alignments.PathAlignments.Multiple.PITools.Multiple_Alignment.MOVABLE_LR;
import static Alignments.PathAlignments.Multiple.PITools.Multiple_Alignment.MOVABLE_N;
import static Alignments.PathAlignments.Multiple.PITools.Multiple_Alignment.MOVABLE_R;
import Alignments.PathAlignments.PairWise.Aligning_Token;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;

/**
 *
 * @author ff
 */
public class AlignTuner {

    private Multiple_Alignment ma;
    private final int k_iter;
    private final int limiting_reason = 2;

    private HashSet<Position> movable;
    private HashMap<Position, Integer> movable_types;
    private HashMap<Position, TuneGroup> movable_groups;
    private HashSet<TuneGroup> groups_total;
    private HashSet<TuneGroup> canbeignored;
    private ArrayList<TuneGroup> groups_alist;
    private ArrayList<Position[]> swapping_pairs;

    private static final double SIMIL_THRESHOLD = 0.5;

    public AlignTuner(Multiple_Alignment ma, int k_iter) {
        this.ma = ma;
        this.k_iter = k_iter;
    }

    public void proceedToTune() throws CloneNotSupportedException {
        this.canbeignored = new HashSet();
        for (int iter = 0; iter < k_iter; iter++) {
            if (!this.tunerIterate(this.ma.getTokens())) {
                break;
            }
        }
    }

    private boolean tunerIterate(ArrayList<Aligning_Token>[] tokens) throws CloneNotSupportedException {
        boolean i_changed = false;
        this.reCompute();
        this.computeMovables();
        this.computeGroups();
        this.movable = new HashSet();
        this.movable_types = new HashMap<>();
        this.identifyMovables(tokens);
        double[] column_scores = this.ma.getColumnScores();
        if (this.tunerSingleIterate(tokens)) {
            i_changed = true;
        }
        if (!i_changed) {
            this.reCompute();
            this.computeMovables();
            this.computeGroups();
            this.movable = new HashSet<>();
            this.movable_types = new HashMap<>();
            boolean g_changed = this.identifyPotentialSwaps(column_scores);
            return g_changed;
        } else return true;
    }

    public void reCompute() {
        this.ma.computeOriginalIndexes();
        this.ma.simmetrizeAlignment();
        this.ma.computeColumnScores();
    }

    public void computeMovables() {
        this.movable = new HashSet<>();
        this.movable_types = new HashMap<>();
        this.identifyMovables(this.ma.getTokens());
    }

    public void computeGroups() {
        this.movable_groups = new HashMap<>();
        this.groups_total = new HashSet<>();
        this.groups_alist = new ArrayList<>();
        this.groups_alist.sort(new TuneGroupComparator());
        this.identifyMovableGroups(this.ma.getTokens());
    }

    private boolean tunerSingleIterate(ArrayList<Aligning_Token>[] tokens) {
        boolean flag = false;
        for (Position movable_el : movable) {
            int i = movable_el.getI();
            int j = movable_el.getJ();
            Object elem = this.ma.getElementAtPos(i, j);
            double this_score = this.ma.getScoreAtPosColumn(elem, i, j);
            double max_score = this_score;
            //we check left
            int js = -1;
            for (int ind = j - 1; ind >= 0 && tokens[i].get(ind).getGap(); ind--) {
                double col_score = this.ma.getScoreAtPosColumn(elem, i, ind);
                if (col_score > max_score) {
                    max_score = col_score;
                    js = ind;
                }
            }
            //we check right
            for (int ind = j + 1; ind < tokens[0].size() && tokens[i].get(ind).getGap(); ind++) {
                double col_score = this.ma.getScoreAtPosColumn(elem, i, ind);
                if (col_score > max_score) {
                    max_score = col_score;
                    js = ind;
                }
            }
            if (js != -1) {
                flag = true;
                this.ma.swapElements(i, j, i, js);
            }
        }
        return flag;
    }

    private void identifyMovables(ArrayList<Aligning_Token>[] tokens) {
        for (int i = 0; i < tokens.length; i++) {
            for (int j = 0; j < tokens[0].size(); j++) {
                Aligning_Token at = tokens[i].get(j);
                if (!at.getGap()) {
                    if (ma.movablePos(i, j)) {
                        Position p = new Position(i, j);
                        movable.add(p);
                        movable_types.put(p, this.ma.movableType(i, j));
                    }
                }
            }
        }
    }

    private void identifyMovableGroups(ArrayList<Aligning_Token>[] tokens) {
        for (int j = 0; j < tokens[0].size(); j++) {
            for (int i = 1; i < tokens.length; i++) {
                //si la posició prèvia és mòbil
                Position pos_prev = new Position(i - 1, j);
                Position pos_curr = new Position(i, j);
                if (movable.contains(pos_prev)) {
                    double simil_prev = -1;
                    // try {
                    //     Object elem_prev = this.ma.getElementAtPos(pos_prev.getI(), pos_prev.getJ());
                    // } catch (Exception ex) {
                    // }
                    try {
                        Object elem_curr = this.ma.getElementAtPos(pos_curr.getI(), pos_curr.getJ());
                        simil_prev = this.ma.getPositionsSimil(elem_curr, i - 1, j);
                    } catch (Exception ex) {
                    }
                    if (this.movable_groups.containsKey(pos_prev)) {
                        TuneGroup tune_p = this.movable_groups.get(pos_prev);
                        //miram si l'element i pot pertanyer al grup o en crea un de nou
                        if (simil_prev >= SIMIL_THRESHOLD && compatibleSense(pos_curr, pos_prev)) {
                            tune_p.addPosition(pos_curr);
                            this.movable_groups.put(pos_curr, tune_p);
                        } else {
                            TuneGroup tune_g = new TuneGroup();
                            tune_g.addPosition(pos_curr);
                            this.movable_groups.put(pos_curr, tune_g);
                            this.groups_total.add(tune_g);
                            this.canbeignored.remove(tune_g);
                            this.groups_alist.add(tune_g);
                        }
                    } else {
                        //miram si l'actual és mòbil
                        if (this.movable.contains(pos_curr)) {
                            //miram si poden anar junts
                            if (simil_prev >= SIMIL_THRESHOLD && this.compatibleSense(pos_curr, pos_prev)) {
                                TuneGroup tune_p = new TuneGroup();
                                tune_p.addPosition(pos_prev);
                                this.movable_groups.put(pos_prev, tune_p);
                                this.groups_total.add(tune_p);
                                this.canbeignored.remove(tune_p);
                                this.groups_alist.add(tune_p);
                                tune_p.addPosition(pos_curr);
                            } else {
                                TuneGroup tune_g = new TuneGroup();
                                tune_g.addPosition(pos_curr);
                                this.movable_groups.put(pos_curr, tune_g);
                                this.groups_total.add(tune_g);
                                this.canbeignored.remove(tune_g);
                                this.groups_alist.add(tune_g);
                            }
                        }
                    }
                } else {
                    //si la posició actual és mòbil
                    if (movable.contains(pos_curr)) {
                        TuneGroup tune_g = new TuneGroup();
                        tune_g.addPosition(pos_curr);
                        this.movable_groups.put(pos_curr, tune_g);
                        this.groups_total.add(tune_g);
                        this.canbeignored.remove(tune_g);
                        this.groups_alist.add(tune_g);
                    }
                }
            }
        }
        ArrayList<TuneGroup> list_copy = (ArrayList<TuneGroup>) this.groups_alist.clone();
        for (TuneGroup g : this.groups_alist) {
            if (g.getPositions().size() <= 1) {
                list_copy.remove(g);
            }
        }
        this.groups_alist = list_copy;
    }

    private boolean identifyPotentialSwaps(double[] column_scores) throws CloneNotSupportedException {
        //System.out.println(this.groups_alist.size());
        boolean to_ret = false;
        //System.out.println(this.groups_alist);
        ArrayList<TuneGroup> list_copy = (ArrayList<TuneGroup>) this.groups_alist.clone();
        for (TuneGroup group : this.groups_alist) {
            //System.out.println("Potential groups resulted");
            if (!this.canbeignored.contains(group) && indentifyPotentialSwapsPerGroup(group, column_scores)) {
                this.canbeignored.add(group);
                to_ret = true;
                list_copy.remove(group);
            }
        }
        this.groups_alist = list_copy;
        return to_ret;
    }

    private boolean indentifyPotentialSwapsPerGroup(TuneGroup group, double[] column_scores) throws CloneNotSupportedException {
        group.initSubSets();
        ArrayList<Position> subset = group.getSSTree().getNextSubSet();
//        System.out.println("Starting to read substeps on a children sized " + subset.size());
        boolean flag = false;
        while (subset != null && subset.size() > 1) {
            //System.out.println("Substep read");
            if (subset.size() > 1 && swapSubSetIfGood(subset, column_scores)) {
                this.reCompute();
                flag = true;
//                System.out.println("Deserves it");
//                group.getSSTree().markCurrChildrenAsExpelled();
//                this.reCompute();
//                this.computeMovables();
//                this.computeGroups();
//                group.updateSubSets();
            }
            //System.out.println("Checking next substep");
            subset = group.getSSTree().getNextSubSet();
//            if (flag) {
//                System.out.println("Next substep is null? " + subset);
//            }
//            if (flag) {
//                System.out.println("In here");
//            }
        }
//        System.out.println("Substeps read");
        return flag;
    }

    private boolean swapSubSetIfGood(ArrayList<Position> positions, double[] column_scores) throws CloneNotSupportedException {
        boolean can_left = true;
        boolean can_right = true;
        for (Position p : positions) {
            int mov_type = MOVABLE_N;
            try {
                mov_type = this.movable_types.get(p);
            } catch (Exception ex) {
            }
            can_left = can_left && (mov_type == MOVABLE_L || mov_type == MOVABLE_LR);
            can_right = can_right && (mov_type == MOVABLE_R || mov_type == MOVABLE_LR);
        }
        double gain_left = -1;
        double gain_right = -1;
        int j_pos = -1;
        if (positions.size() > 0) {
            j_pos = positions.get(0).getJ();
        }
        can_left = can_left && j_pos != -1;
        can_right = can_right && j_pos != -1;
        double min_l = 0;
        double min_r = 0;
        if (can_left) {
            gain_left = 0;
            double score_loss = 0;
            for (Position p : positions) {
                try {
                    score_loss += this.ma.getScoreAtPosColumn(p.getI(), p.getJ(), p.getJ());
                } catch (Exception ex) {
                    can_left = false;
                }
            }
            this.swapPositions(positions, false, false);
            double score_gain = 0;
            for (Position p : positions) {
                Position p_l = new Position(p.getI(), p.getJ() - 1);
                try {
                    score_gain += this.ma.getScoreAtPosColumn(p_l.getI(), p_l.getJ(), p_l.getJ());
                } catch (Exception ex) {
                }
            }
            gain_left = score_gain - score_loss;
            this.swapPositions(positions, false, true);
//            al_copy_left.computeColumnScores();
//            double[] new_scores_l = al_copy_left.getColumnScores();
//            double original_gain = new_scores_l[j_pos] - column_scores[j_pos];
//            gain_left = (new_scores_l[j_pos - 1] - new_scores_l[j_pos - 1]) + original_gain;
        } else {
            if (can_right) {
                gain_right = 0;
                double score_loss = 0;
                for (Position p : positions) {
                    try {
                        score_loss += this.ma.getScoreAtPosColumn(p.getI(), p.getJ(), p.getJ());
                    } catch (Exception ex) {
                        can_right = false;
                    }
                }
                this.swapPositions(positions, true, false);
                double score_gain = 0;
                for (Position p : positions) {
                    Position p_r = new Position(p.getI(), p.getJ() + 1);
                    try {
                        score_gain += this.ma.getScoreAtPosColumn(p_r.getI(), p_r.getJ(), p_r.getJ());
                    } catch (Exception ex) {
                    }
                }
                this.swapPositions(positions, true, true);
                gain_right = (score_gain - score_loss);
//                al_copy_right.computeColumnScores();
//                double[] new_scores_r = al_copy_right.getColumnScores();
//                double original_gain = column_scores[j_pos + 1] - column_scores[j_pos];
//                gain_right = (new_scores_r[j_pos + 1] - new_scores_r[j_pos]) + original_gain;

            }
        }
        if (can_left && gain_left >= gain_right) {
            if (gain_left > min_l) {
                // System.out.println("Return with gain left " + gain_left);
                this.swapPositions(positions, false, false);
                return true;
            }
        } else {
            if (can_right && gain_right > min_r) {
                // System.out.println("Return with gain right " + gain_right);
                this.swapPositions(positions, true, false);
                return true;
            }
        }
        return false;
    }

    public void swapPositions(ArrayList<Position> positions, boolean right_left, boolean reverse) {
        positions.forEach(p -> {
            Position p_r = new Position(p.getI(), p.getJ() + 1);
            Position p_l = new Position(p.getI(), p.getJ() - 1);
            Position p_a = right_left ? p_r : p_l;
            if (!reverse) {
                this.ma.swapElements(p.getI(), p.getJ(), p_a.getI(), p_a.getJ());
            } else {
                this.ma.swapElements(p_a.getI(), p_a.getJ(), p.getI(), p.getJ());
            }
        });
    }

    public boolean compatibleSense(Position p1, Position p2) {
        return this.movable_types.containsKey(p1) && this.movable_types.containsKey(p2)
                && (Objects.equals(this.movable_types.get(p1), this.movable_types.get(p2))
                || this.movable_types.get(p2) == MOVABLE_LR
                || this.movable_types.get(p1) == MOVABLE_LR);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.PathAlignments.Multiple.PITools;

import Auxiliar.Comparison.Compare_Basic;
import Alignments.PathAlignments.PairWise.Aligning_Token;
import Auxiliar.Utils;
import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class Profiled_Module {
    private ArrayList<Integer> profile;
    private ArrayList<Integer> new_profile;
    private ArrayList<Aligning_Token> alignment;
    private boolean bear_new_gaps;
    private ArrayList<Integer> added_gaps;
    private ArrayList<Integer> added_gaps_nt;
    private ArrayList<Integer> combined_gaps;
    public Profiled_Module(ArrayList<Integer> profile, ArrayList<Aligning_Token> alignment, boolean bear_new_gaps) {
        this.profile = profile;
        this.new_profile = new ArrayList();
        this.alignment = alignment;
        this.bear_new_gaps = bear_new_gaps;
        this.added_gaps = new ArrayList();
        this.added_gaps_nt = new ArrayList();
        this.combined_gaps = new ArrayList();
        proceedToAnalyze();
        proceedToCombine();
    }
    public ArrayList<Integer> getModifiedProfile() {
        return new_profile;
    }
    public ArrayList<Aligning_Token> getModifiedAlignment() {
        return alignment;
    }
    public ArrayList<Integer> getAddedGaps() {
        return added_gaps;
    }
    public ArrayList<Integer> getAddedGapsNT() {
        return added_gaps;
    }
    public ArrayList<Integer> getCombinedGaps() {
        return combined_gaps;
    }
    private void proceedToAnalyze() {
        
        int i = 0;
        int gap_cnt = 0;
        
        while (i < alignment.size()) {
            
            int prf_siz = new_profile.size();
            int ind = i+prf_siz;
            int rel_ind = ind-gap_cnt;
            boolean valid_prf = !profile.isEmpty();
            
            if (valid_prf && bear_new_gaps && profile.get(0)==rel_ind) {
                profile.remove(0);
                new_profile.add(ind);
            } 
            else if (valid_prf && !bear_new_gaps && profile.get(0)==ind) {
                profile.remove(0);
                new_profile.add(ind);
            } 
            else {
                if (alignment.get(i).getGap()) {
                    added_gaps.add(ind);
                    added_gaps_nt.add(i);
                    gap_cnt++;
                }
                i++;
            }
            
        }
        
        new_profile.sort(new Compare_Basic());
    }
    private void proceedToCombine() {
        for (int i : new_profile) {
            if (i < alignment.size()) alignment.add(i,new Aligning_Token(true));
            else alignment.add(new Aligning_Token(true));
        }
        combined_gaps = Utils.combineLists(new_profile, added_gaps);
        combined_gaps.sort(new Compare_Basic());
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.PathAlignments.Multiple.PITools;

import Forest.Path;
import java.util.Objects;

/**
 *
 * @author ASUS
 */
public class Pair {
    
    private Path o1;
    private Path o2;
    public Pair(Path o1, Path o2) {
        this.o1 = o1;
        this.o2 = o2;
    }
    public Path[] getElems() {
        return new Path[]{o1,o2};
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.o1);
        hash = 53 * hash + Objects.hashCode(this.o2);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pair other = (Pair) obj;
        if (!Objects.equals(this.o1, other.o1)) {
            return false;
        }
        if (!Objects.equals(this.o2, other.o2)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return o1.toString()+" -p- "+o2.toString();
    }
    
}

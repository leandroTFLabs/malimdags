/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.PathAlignments.Multiple.PITools;

import Auxiliar.Utils;
import Forest.Path;
import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author ASUS
 */
public class Partial_Alignment {
    
    private ArrayList<Path> aligned_nodes;
    private HashSet<Path> nodes_set;
    
    public Partial_Alignment() {
        this.aligned_nodes = new ArrayList();
        this.nodes_set = new HashSet();
    }
    
    public ArrayList<Path> getAlignedNodes() {
        return aligned_nodes;
    }
    
    public void addNode(Path node) {
        aligned_nodes.add(node);
        if (nodes_set.contains(node)) nodes_set.add(node);
    }
    
    public boolean containsNode(Object node) {
        return nodes_set.contains(node);
    }
    
    public boolean freeNode(Object node) {
        return isFirstNode(node) || isLastNode(node);
    }
    
    public boolean isFirstNode(Object node) {
        return aligned_nodes.get(0).equals(node);
    }
    
    public boolean isLastNode(Object node) {
        return aligned_nodes.get(aligned_nodes.size()-1).equals(node);
    }
    
    public void reverseAlignment() {
        this.aligned_nodes = Utils.revertArrayList(aligned_nodes);
    }
    
    public static Partial_Alignment appendTwoPartial(Partial_Alignment pa1, Partial_Alignment pa2) {
        Partial_Alignment pa = new Partial_Alignment();
        for (Path node : pa1.getAlignedNodes()) {
            pa.addNode(node);
        }
        for (Path node : pa2.getAlignedNodes()) {
            pa.addNode(node);
        }
        return pa;
    }
}

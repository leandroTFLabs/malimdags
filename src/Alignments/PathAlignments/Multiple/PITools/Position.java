/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.PathAlignments.Multiple.PITools;

/**
 *
 * @author ff
 */
public class Position {
    private int i;
    private int j;
    public Position(int i, int j) {
        this.i = i;
        this.j = j;
    }
    public int getI() {return this.i;}
    public int getJ() {return this.j;}

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + this.i;
        hash = 41 * hash + this.j;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Position other = (Position) obj;
        if (this.i != other.i) {
            return false;
        }
        if (this.j != other.j) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{" + i + ", " + j + '}';
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.PathAlignments.Multiple.PITools;

import Alignments.PathAlignments.PairWise.Aligning_Token;
import Stats.StatsMaker;
import java.util.ArrayList;
import Auxiliar.Comparison.MBB_Simil_Data;

/**
 *
 * @author ASUS
 * @param <T>
 */
public final class Multiple_Alignment<T> implements Cloneable {

    public final static int MOVABLE_N = -1;
    public final static int MOVABLE_L = 0;
    public final static int MOVABLE_R = 1;
    public final static int MOVABLE_LR = 2;

    private final ArrayList<T> nodes;
    private final ArrayList<Aligning_Token>[] tokens;

    private int[][] original_indexes;
    private String max_string_size = "-";
    private ArrayList<ArrayList<String>> rows_elements;
    private MBB_Simil_Data simils;
    private double[] column_scores;

    public Multiple_Alignment(ArrayList<T> nodes, ArrayList<Aligning_Token>[] tokens) {
        this.nodes = nodes;
        this.tokens = tokens;
        simmetrizeAlignment();
        this.original_indexes = new int[this.tokens.length][this.tokens[0].size()];
        computeOriginalIndexes();
        this.column_scores = new double[tokens[0].size()];
    }

    public ArrayList<T> getNodes() {
        return nodes;
    }

    public ArrayList<Aligning_Token>[] getTokens() {
        return tokens;
    }

    public String getMaxSizeDesc() {
        return max_string_size;
    }

    public double[] getColumnScores() {
        return column_scores;
    }

    public double[][] getFocusedScores(Object element) {
        double[][] scores_matrix = new double[tokens.length][tokens[0].size()];
        for (int i = 0; i < tokens.length; i++) {
            for (int j = 0; j < tokens[0].size(); j++) {
                Aligning_Token elem = tokens[i].get(j);
                if (!elem.getGap()) {
                    int original_ind = original_indexes[i][j];
                    Object orig_elem = rows_elements.get(i).get(original_ind);
                    scores_matrix[i][j] = getObjectsSimil(orig_elem, element);
                } else {
                    scores_matrix[i][j] = 0.0;
                }
            }
        }
        return scores_matrix;
    }

    public ArrayList<ArrayList<String>> getElemsDescriptions() {
        ArrayList<ArrayList<String>> to_ret = new ArrayList();
        int i = 0;
        for (ArrayList<Aligning_Token> al : tokens) {
            ArrayList<String> alst = new ArrayList();
            int j = 0;
            for (Aligning_Token t : al) {
                if (t.getGap()) {
                    alst.add("-");
                } else {
                    int ind = original_indexes[i][j];
                    alst.add(rows_elements.get(i).get(ind));
                }
                j++;
            }
            to_ret.add(alst);
            i++;
        }
        return to_ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    public void provideAdditionalInfo(ArrayList<ArrayList<String>> rows_elements, MBB_Simil_Data simils) {
        this.rows_elements = rows_elements;
        for (ArrayList al : rows_elements) {
            for (Object o : al) {
                if (o.toString().length() > max_string_size.length() || !o.toString().equals("-") && max_string_size.equals("-")) {
                    max_string_size = o.toString();
                }
            }
        }
        this.simils = simils;
        this.computeColumnScores();
    }

    public void superTuning(int k_iter) throws CloneNotSupportedException {
        tuneAlignment(k_iter);
        polishEmptyCs();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    public void simmetrizeAlignment() {
        int max = Integer.MIN_VALUE;
        for (ArrayList l : tokens) {
            if (l.size() > max) {
                max = l.size();
            }
        }
        for (int i = 0; i < tokens.length; i++) {
            ArrayList l = tokens[i];
            while (l.size() < max) {
                l.add(new Aligning_Token(true));
            }
            tokens[i] = l;
        }
    }

    public void computeOriginalIndexes() {
        this.original_indexes = new int[tokens.length][tokens[0].size()];
        int cnt = 0;
        for (ArrayList<Aligning_Token> tokseq : tokens) {
            int gap = 0;
            int cnt_s = 0;
            for (Aligning_Token at : tokseq) {
                int ind = cnt_s - gap;
                if (at.getGap()) {
                    gap++;
                    original_indexes[cnt][cnt_s] = -1;
                } else {
                    original_indexes[cnt][cnt_s] = ind;
                }
                cnt_s++;
            }
            cnt++;
        }
    }

    private void tuneAlignment(int k_iter) throws CloneNotSupportedException {
//        for (int iter = 0; iter < k_iter; iter++) {
//            //we check movable elements
//            ArrayList<int[]> movable = new ArrayList();
//            for (int i = 0; i < tokens.length; i++) {
//                for (int j = 0; j < tokens[0].size(); j++) {
//                    Aligning_Token at = tokens[i].get(j);
//                    if (!at.getGap()) {
//                        if (movablePos(i,j)) movable.add(new int[]{i,j});
//                    }
//                }
//            }
//            //we check better choices for movable elements
//            for (int[] movable_el : movable) {
//                int i = movable_el[0];
//                int j = movable_el[1];
//                Object elem = getElementAtPos(i, j);
//                double this_score = getScoreAtPosColumn(elem, i, j);
//                double max_score = this_score;
//                //we check left
//                int js = -1;
//                for (int ind = j-1; ind >= 0 && tokens[i].get(ind).getGap(); ind--) {
//                    double col_score = getScoreAtPosColumn(elem, i, ind);
//                    if (col_score > max_score) {
//                        max_score = col_score;
//                        js = ind;
//                    }
//                }
//                //we check right
//                for (int ind = j+1; ind < tokens[0].size() && tokens[i].get(ind).getGap(); ind++) {
//                    double col_score = getScoreAtPosColumn(elem, i, ind);
//                    if (col_score > max_score) {
//                        max_score = col_score;
//                        js = ind;
//                    }
//                }
//                //if it worth it, we swap
//                if (js != -1) {
//                    swapElements(i,j,i,js);
//                    original_indexes[i][js] = original_indexes[i][j];
//                    original_indexes[i][j] = -1;
//                }
//            }
//        }
        AlignTuner tuner = new AlignTuner(this, k_iter);
        tuner.proceedToTune();
    }

    private void polishEmptyCs() {
        ArrayList<Integer> to_delete_cols = new ArrayList();
        for (int j = tokens[0].size() - 1; j >= 0; j--) {
            boolean empty = true;
            int i = 0;
            for (ArrayList<Aligning_Token> token : tokens) {
                if (original_indexes[i][j] != -1) {
                    empty = false;
                    break;
                }
                i++;
            }
            if (empty) {
                to_delete_cols.add(j);
            }
        }
        to_delete_cols.forEach(j -> {
            for (ArrayList<Aligning_Token> token : tokens) {
                token.remove(token.get(j));
            }
        });
        computeOriginalIndexes();
        computeColumnScores();
    }

    public double getScoreAtPosColumn(Object elem, int i, int j) {
        int cnt = 0;
        double sum = 0.0;
        for (int ind = 0; ind < tokens.length; ind++) {
            if (ind != i) {
                sum += getPositionsSimil(elem, ind, j);
                cnt++;
            }
        }
        return sum / ((double) cnt);
    }

    public double getScoreAtPosColumn(int i0, int j0, int j) {
        int cnt = 0;
        double sum = 0.0;
        for (int ind = 0; ind < tokens.length; ind++) {
            if (ind != i0) {
                sum += getPositionsSimil(this.getElementAtPos(i0, j0), ind, j);
                cnt++;
            }
        }
        return sum / ((double) cnt);
    }

    public double getPositionsSimil(Object elem, int i1, int j1) {
        if (!tokens[i1].get(j1).getGap()) {
            return getObjectsSimil(elem, getElementAtPos(i1, j1));
        } else {
            return 0.0;
        }
    }

    public void swapElements(int i0, int j0, int i1, int j1) {
        Aligning_Token temp = tokens[i0].get(j0);
        tokens[i0].set(j0, tokens[i1].get(j1));
        tokens[i1].set(j1, temp);
        if (j1 != -1) {
            int oind_temp = original_indexes[i1][j1];
            original_indexes[i1][j1] = original_indexes[i0][j0];
            original_indexes[i0][j0] = oind_temp;
        }
    }

    public boolean movablePos(int i, int j) {
        int mt = this.movableType(i, j);
        return mt == MOVABLE_L || mt == MOVABLE_R || mt == MOVABLE_LR;
    }

    public boolean movableLeft(int i, int j) {
        int mt = this.movableType(i, j);
        return mt == MOVABLE_L || mt == MOVABLE_LR;
    }

    public boolean movableRight(int i, int j) {
        int mt = this.movableType(i, j);
        return mt == MOVABLE_R || mt == MOVABLE_LR;
    }

    public int movableType(int i, int j) {
        boolean movable_l = j > 0 ? !tokens[i].get(j).getGap() && tokens[i].get(j - 1).getGap() : false;
        boolean movable_r = j < tokens[i].size() - 1 ? !tokens[i].get(j).getGap() && tokens[i].get(j + 1).getGap() : false;
        if (movable_l || movable_r) {
            if (movable_l) {
                if (movable_r) {
                    return MOVABLE_LR;
                } else {
                    return MOVABLE_L;
                }
            } else {
                if (movable_r) {
                    return MOVABLE_R;
                } else {
                    return MOVABLE_N;
                }
            }
        } else {
            return MOVABLE_N;
        }
    }

    public Object getElementAtPos(int i, int j) {
        int ind = original_indexes[i][j];
        return rows_elements.get(i).get(ind);
    }

    public void computeColumnScores() {
        this.column_scores = new double[tokens[0].size()];
        for (int j = 0; j < tokens[0].size(); j++) {
            ArrayList<Double> scores = new ArrayList();
            ArrayList<String> to_pair = new ArrayList();
            for (int i = 0; i < tokens.length; i++) {
                if (original_indexes[i][j] != -1) {
                    int ind = original_indexes[i][j];
                    to_pair.add(rows_elements.get(i).get(ind));
                } else {
                    to_pair.add("-");
                }
            }
            for (int k = 0; k < to_pair.size(); k++) {
                for (int r = k + 1; r < to_pair.size(); r++) {
                    if (!tokens[k].get(j).getGap() && !tokens[r].get(j).getGap()) {
                        scores.add(getObjectsSimil(to_pair.get(k), to_pair.get(r)));
                    } else {
                        scores.add(0.0);
                    }
                }
            }
            StatsMaker sm = new StatsMaker(scores);
            sm.calculaStats();
            column_scores[j] = sm.getMitjana();
        }
    }

    private double getObjectsSimil(Object o1, Object o2) {
        return simils.getSimilMBBs((String) o1, (String) o2);
    }

    @Override
    public Multiple_Alignment clone() throws CloneNotSupportedException {
        return (Multiple_Alignment) super.clone();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.PathAlignments.Multiple.PITools;

import Alignments.PathAlignments.PairWise.Aligning_Token;
import Alignments.PathAlignments.PairWise.PW_Dynamic_Sequence_Aligner;
import Auxiliar.Utils;
import Forest.Path;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author ASUS
 */
public class GreedySorting {
    
    private ArrayList<Partial_Alignment> align_parts;
    private HashMap<Path,Integer> nodes_indexes;
    private Multiple_Alignment<Path> final_alignment;
    
    public GreedySorting() {
        this.align_parts = new ArrayList();
        this.nodes_indexes = new HashMap();
    }
    
    public Multiple_Alignment<Path> getFinalAlignment() {
        return final_alignment;
    }
    
    public boolean nodesPairIsPossible(Pair pair) {
        boolean nested_1 = nodes_indexes.containsKey(pair.getElems()[0]);
        boolean nested_2 = nodes_indexes.containsKey(pair.getElems()[1]);
        int ind_1,ind_2;
        ind_1 = -1;
        ind_2 = -2;
        if (nested_1) {
            ind_1 = nodes_indexes.get(pair.getElems()[0]);
            if (!align_parts.get(ind_1).freeNode(pair.getElems()[0])) return false;
        }
        if (nested_2) {
            ind_2 = nodes_indexes.get(pair.getElems()[1]);
            if (!align_parts.get(ind_2).freeNode(pair.getElems()[1])) return false;
        }
        if (ind_1 == ind_2) return false;
        return true;
    }
    
    public void addNodesPair(Pair pair) {
        boolean nested_1 = nodes_indexes.containsKey(pair.getElems()[0]);
        boolean nested_2 = nodes_indexes.containsKey(pair.getElems()[1]);
        if (nested_1 && nested_2) {
            //we must join two partial alignments
            //System.out.println("two partial");
            int ind_1 = nodes_indexes.get(pair.getElems()[0]);
            int ind_2 = nodes_indexes.get(pair.getElems()[1]);
            joinTwoAlignments(pair.getElems()[0],pair.getElems()[1],ind_1,ind_2);
        } else if (nested_1) {
            //we must join an independent node with an alignment
            //System.out.println("indep 2 with align");
            int ind = nodes_indexes.get(pair.getElems()[0]);
            joinNodeToAlignment(pair.getElems()[0],pair.getElems()[1],1,ind);
        } else if (nested_2) {
            //we must join an independent node with an alignment
            //System.out.println("indep 1 with align");
            int ind = nodes_indexes.get(pair.getElems()[1]);
            joinNodeToAlignment(pair.getElems()[0],pair.getElems()[1],0,ind);
        } else {
            //we must join two independent nodes
            //System.out.println("two indep");
            joinTwoIndepndNodes(pair.getElems()[0],pair.getElems()[1]);
        }
    }
    
    private void joinTwoAlignments(Path o1, Path o2, int ind_1, int ind_2) {
        if (ind_2 < ind_1) {
            Path temp = o2;
            o1 = o2;
            o2 = temp;
        }
        int min_ind = Math.min(ind_1, ind_2);
        int max_ind = Math.max(ind_1, ind_2);
        Partial_Alignment min_align = align_parts.get(min_ind);
        Partial_Alignment max_align = align_parts.get(max_ind);
        if (min_align.isFirstNode(o1)) min_align.reverseAlignment();
        if (max_align.isLastNode(o2)) max_align.reverseAlignment();
        min_align = Partial_Alignment.appendTwoPartial(min_align, max_align);
        align_parts.remove(max_ind);
        align_parts.set(min_ind, min_align);
        //now we have to recompose indexes
        for (Path node : nodes_indexes.keySet()) {
            int ind = nodes_indexes.get(node);
            if (ind == max_ind) nodes_indexes.put(node,min_ind);
            if (ind > max_ind) nodes_indexes.put(node,ind - 1);
        }
    }
    
    private void joinNodeToAlignment(Path o1, Path o2, int indep, int ind) {
        Path indepnode, jointnode;
        if (indep==0) {
            indepnode = o1;
            jointnode = o2;
        } else {
            indepnode = o2;
            jointnode = o1;
        }
        Partial_Alignment align = align_parts.get(ind);
        if (align.isFirstNode(jointnode)) align.reverseAlignment();
        align.addNode(indepnode);
        align_parts.set(ind, align);
        nodes_indexes.put(indepnode, ind);
    }
    
    private void joinTwoIndepndNodes(Path o1, Path o2) {
        Partial_Alignment pa = new Partial_Alignment();
        int ind = align_parts.size();
        pa.addNode(o1);
        pa.addNode(o2);
        align_parts.add(pa);
        nodes_indexes.put(o1, ind);
        nodes_indexes.put(o2, ind);
    }
    
    public void calcFinalAlignment(HashMap<Pair,PW_Dynamic_Sequence_Aligner> align_info) {
        ArrayList<Path> nodes = align_parts.get(0).getAlignedNodes();
        
        int siz = nodes.size();
        
        ArrayList[] to_ret = new ArrayList[siz];
        
        ArrayList<Integer> profile = new ArrayList();
        
        for (int i = 0; i < nodes.size() - 1; i++) {
            Path o1 = nodes.get(i);
            Path o2 = nodes.get(i+1);
            Pair pair = new Pair(o1,o2);
            
            boolean swapped = false;
            if (!align_info.containsKey(pair)) {
                pair = new Pair(o2,o1);
                swapped = true;
            }
            
            ArrayList<Aligning_Token>[] new_alig = align_info.get(pair).getAlignedSequences();
            int first = 0; int secnd = 1;
            if (swapped) {
                first = 1; secnd = 0;
            }
            
            ArrayList seq1 = new_alig[first];
            ArrayList seq2 = new_alig[secnd];
            
            /*first, we'll profile the first sequence
                with the incoming profile 
                and recover the modified profile and new gaps*/
            Profiled_Module pseq1 = new Profiled_Module(Utils.copyList(profile),seq1,true);
            ArrayList<Integer> tr_profile = pseq1.getModifiedProfile();
            //and we'll profile the second sequence with the modified profile
            Profiled_Module pseq2 = new Profiled_Module(Utils.copyList(tr_profile),seq2,false);
            //we'll set the resulting alignments as part of the final alignment set
            to_ret[i] = pseq1.getModifiedAlignment();
            to_ret[i+1] = pseq2.getModifiedAlignment();
            
            /*
            Now, we're going to combine the existing transformed profile with new gaps in sequence 2
            */
            profile = pseq2.getCombinedGaps();
            
            /*
            Finally, we're going to re-combine previous alignments with new gaps added on first sequence
            */
            ArrayList<Integer> new_gaps_seq1 = pseq1.getAddedGaps();
            for (int j = i-1; j >= 0; j--) {
                Profiled_Module prevseq = new Profiled_Module(Utils.copyList(new_gaps_seq1),Utils.copyList(to_ret[j]),false);
                to_ret[j] = prevseq.getModifiedAlignment();
            }
        }
        
        this.final_alignment = new Multiple_Alignment<Path>(nodes,to_ret);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.PathAlignments.Multiple;

import Alignments.PathAlignments.Multiple.PITools.MT_Progressive;
import Alignments.PathAlignments.Multiple.PITools.Multiple_Alignment;
import Alignments.PathAlignments.Multiple.PITools.Paired_Module;
import Alignments.PathAlignments.Multiple.PITools.Pair;
import Auxiliar.Comparison.MBB_Simil_Data;
import Auxiliar.ViewData.PlainText.Align_To_PlainFile;
import Forest.Path;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class MA_Path_Aligner {
    private ArrayList<Path> paths_to_align;
    private final int K_ITER = 50;
    /*
    DATA RECEIVED
    */
    private MBB_Simil_Data mbb_simils;
    //
    /*
    DATA TO PROVIDE TO ALIGNER
    */
    //pre-align
    private MT_Progressive aligner;
    private ArrayList<Paired_Module> pairs_aligned;
    //post-align
    private Multiple_Alignment<Path> ma;
    private ArrayList<ArrayList<String>> mbb_desc;
    //markup
    private ArrayList<String> paths_desc;
    private String group_id;
    private String group_desc;
    private int align_number;
    private final String dir_name = System.getProperty("user.dir").concat("/DATA/calculated/MULTIPLE-ALIGNMENTS/");
    private String derived_dir_name;
    //
    private double pre_score = 0.0;
    public MA_Path_Aligner(String group_id, String group_desc, int align_number, MBB_Simil_Data mbb_simils, ArrayList<Path> paths_to_align) throws IOException, CloneNotSupportedException {
        
        File maindir = new File(dir_name);
        if (!maindir.exists()) maindir.mkdir();
        this.derived_dir_name = dir_name.concat(group_id+"/");
        File dervdir = new File(derived_dir_name);
        if (!dervdir.exists()) dervdir.mkdir();
        
        //Path score = paths_to_align.get(paths_to_align.size() - 1);
        
        this.paths_to_align = paths_to_align;
        this.mbb_simils = mbb_simils;
        this.group_id = group_id;
        this.group_desc = group_desc;
        this.align_number = align_number;
        prepareAlignment();
        executeAlignment();
        refineAlignment();
        dumpAlignment();
    }

    private void prepareAlignment() {
        pairs_aligned = new ArrayList();
        for (int i = 0; i < paths_to_align.size(); i++) {
            for (int j = i+1; j < paths_to_align.size(); j++) {
                double[][] simils_matrix = getSimilsMatrix(paths_to_align.get(i),paths_to_align.get(j));
                Paired_Module pm = new Paired_Module(new Pair(paths_to_align.get(i),paths_to_align.get(j)),simils_matrix);
                pairs_aligned.add(pm);
            }
        }
    }

    private void executeAlignment() {
        aligner = new MT_Progressive(pairs_aligned);
        ma = aligner.getFinalAlignment();
    }

    private void refineAlignment() throws CloneNotSupportedException {
        mbb_desc = new ArrayList();
        paths_desc = new ArrayList();
        ma.getNodes().stream().map(p -> {
            paths_desc.add(p.toString());
            return p;
        }).map(p -> {
            ArrayList<String> local = new ArrayList();
            p.getNodesCami().forEach(n -> {
                local.add(n.toString());
            });
            return local;
        }).forEachOrdered(local -> {
            mbb_desc.add(local);
        });
        ma.provideAdditionalInfo(mbb_desc, mbb_simils);
        ma.superTuning(K_ITER);
    }

    private void dumpAlignment() throws IOException {
        Align_To_PlainFile align_To_PlainFile = new Align_To_PlainFile(derived_dir_name+group_id+"_"+align_number+".txt",paths_desc,ma,false,null);
    }

    private double[][] getSimilsMatrix(Path get, Path get0) {
        int n = get.getPathSize();
        int m = get0.getPathSize();
        double[][] simils_matrix = new double[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                simils_matrix[i][j] = mbb_simils.getSimilMBBs(get.getNodesCami().get(i).toString(), get0.getNodesCami().get(j).toString());
            }
        }
        return simils_matrix;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.PathAlignments.Multiple;

import Auxiliar.Comparison.MBB_Simil_Data;
import Forest.Path;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class MA_Group_Aligner extends Thread {
    
    private String group_desc_line;
    private ArrayList<ArrayList<Path>> path_lines = new ArrayList();
    
    @Override
    public void run() {
        processGroupInfo(group_desc_line);
        try {
            processPathLines(path_lines);
        } catch (IOException | CloneNotSupportedException ex) {
            Logger.getLogger(MA_Group_Aligner.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private String group_id;
    private String group_ds;
    private MBB_Simil_Data mbb_simils;
    
    public MA_Group_Aligner(String group_desc_line, ArrayList<ArrayList<Path>> path_lines, MBB_Simil_Data mbb_simils) throws IOException, CloneNotSupportedException {
        this.mbb_simils = mbb_simils;
        this.group_desc_line = group_desc_line;
        this.path_lines = path_lines;
//        processGroupInfo(group_desc_line);
//        processPathLines(path_lines);
    }

    private void processGroupInfo(String group_desc_line) {
        String tr_gdl = group_desc_line.replace(" ","").replace("\t","");
        String[] both_parts = tr_gdl.split("=");
        this.group_id = both_parts[0];
        this.group_ds = both_parts[1];
    }

    private void processPathLines(ArrayList<ArrayList<Path>> path_lines) throws IOException, CloneNotSupportedException {
        int cnt = 0;
        for (ArrayList<Path> al : path_lines) {
            MA_Path_Aligner mpa = new MA_Path_Aligner(group_id,group_ds,cnt,mbb_simils,al);
            cnt++;
        }
    }
}

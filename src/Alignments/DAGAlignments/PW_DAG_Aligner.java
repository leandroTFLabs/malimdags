/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.DAGAlignments;

import Auxiliar.Comparison.MBB_Simil_Data;
import Auxiliar.Clustering.Path_Label;
import Forest.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author ASUS
 */
public class PW_DAG_Aligner {
    /*
     * ATRIBUTS
     */
    private ArrayList<String> dag_names;
    private ArrayList<Labeled_Structure> dags;
    private MBB_Simil_Data mbb_simils;
    private ArrayList<Path_Label> existing_labels;
    private HashMap<Path_Label, ArrayList<String>> sorted_by_now;
    private Labeled_Alignment alignment;

    private ArrayList<Path_Label> effective_labels;
    private ArrayList<Hungarian_Path_Combiner> await_threads;

    private String getStrucDagName(int index) {
        return dag_names.get(index);
    }

    ///////////////////////////////////////////////
    /*
     * CONSTRUCTORS
     */
    public PW_DAG_Aligner(ArrayList<String> dag_names, ArrayList<Labeled_Structure> dags,
            Labeled_Alignment former_alignment, int max_sample_size, MBB_Simil_Data mbb_simils,
            HashMap<Path_Label, ArrayList<String>> sorted_dags) {
        this.dag_names = dag_names;
        this.dags = dags;
        this.mbb_simils = mbb_simils;
        this.alignment = former_alignment == null ? new Labeled_Alignment() : former_alignment;
        this.sorted_by_now = sorted_dags;
        this.existing_labels = new ArrayList<>();
        for (Labeled_Structure ls : dags) {
            Set<Path_Label> dLabs = ls.getLabelSet();
            for (Path_Label lab : dLabs) {
                if (!existing_labels.contains(lab))
                    existing_labels.add(lab);
            }
        }
        processAlignments();
    }

    ///////////////////////////////////////////////
    /*
     * GETTERS
     */
    public HashMap<Path_Label, ArrayList<String>> getSortedByNow() {
        return sorted_by_now;
    }

    public Labeled_Alignment getAlignment() {
        return alignment;
    }

    ///////////////////////////////////////////////
    /*
     * SETTERS
     */
    ///////////////////////////////////////////////
    /*
     * ALTRES MÈTODES AUXILIARS
     */
    private void processAlignments() {
        this.effective_labels = new ArrayList<>();
        this.await_threads = new ArrayList<>();
        for (Path_Label pl : existing_labels) {
            processLabeledAlignments(pl);
        }
        int cnt = 0;
        for (Hungarian_Path_Combiner t : await_threads) {
            try {
                t.join();
                System.out.println("Thread over");
                ArrayList<Path[]> aligned_pairs = t.getAssignedPairs();
                ArrayList<Double> paired_scores = t.getScores();
                int cnt_sub = 0;
                for (Path[] pair : aligned_pairs) {
                    // System.out.println("Adding "+Arrays.toString(pair));
                    alignment.addLabeledPair(effective_labels.get(cnt), pair[0], pair[1], paired_scores.get(cnt_sub));
                    cnt_sub++;
                }
            } catch(InterruptedException ex) {}
            cnt++;
        }
    }

    private Labeled_Structure getNextMaxForLabel(Path_Label pl) {
        int max = -1;
        int max_index = -1;
        String max_dag_name = "";
        for (int i = 0; i < dags.size(); i++) {
            String dag_name = this.getStrucDagName(i);
            if (this.sorted_by_now.get(pl) == null) {
                this.sorted_by_now.put(pl, new ArrayList<>());
            } else {
                if (!this.sorted_by_now.get(pl).contains(dag_name)) {
                    int pl_num = dags.get(i).getPathsForLabel(pl).size();
                    if (pl_num > max) {
                        max_index = i;
                        max = pl_num;
                        max_dag_name = dag_name;
                    }
                }
            }
        }
        if (max_index == -1) {
            return null;
        } else {
            ArrayList<String> occupied = this.sorted_by_now.get(pl);
            occupied.add(max_dag_name);
            this.sorted_by_now.put(pl, occupied);
            return dags.get(max_index);
        }
    }

    private void processLabeledAlignments(Path_Label pl) {
        if (this.sorted_by_now.get(pl) == null) {
            this.sorted_by_now.put(pl, new ArrayList<>());
        }
        Labeled_Structure max1;
        if (this.sorted_by_now.get(pl).size() == 0) max1 = getNextMaxForLabel(pl);
        else {
            max1 = this.dags.get(
                this.dag_names.indexOf(
                    this.sorted_by_now.get(pl).get(
                        this.sorted_by_now.get(pl).size() - 1
                    )
                )
            );
        }
        Labeled_Structure max2 = getNextMaxForLabel(pl);
        if (max1 != null && max2 != null) {
            ArrayList<Path> pld1 = max1.getPathsForLabel(pl);
            ArrayList<Path> pld2 = max2.getPathsForLabel(pl);
            if (pld1.size() > 0 && pld2.size() > 0) {
                Hungarian_Path_Combiner munkres_aligner = new Hungarian_Path_Combiner(pld1, pld2, mbb_simils);
                munkres_aligner.start();
                this.effective_labels.add(pl);
                this.await_threads.add(munkres_aligner);
            }
        }
    }
    ///////////////////////////////////////////////
}

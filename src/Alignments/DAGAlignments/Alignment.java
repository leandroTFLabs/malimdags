/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.DAGAlignments;

import Forest.Path;
import Stats.StatsMaker;
import java.util.ArrayList;
import java.util.Comparator;

/**
 *
 * @author ASUS
 */
public class Alignment {
    private ArrayList<ArrayList<Path>> assignments;
    private ArrayList<ArrayList<Double>> scores;
    
    private class Compare_Align_Score implements Comparator<Alignment_Score> {

        @Override
        public int compare(Alignment_Score o1, Alignment_Score o2) {
            return Double.compare(o2.getMeanScore(), o1.getMeanScore());
        }
        
    }
    
    private class Alignment_Score {
        private ArrayList<Path> assign;
        private ArrayList<Double> scores;
        private double mean_score;
        public Alignment_Score(ArrayList<Path> assign, ArrayList<Double> scores, double mean_score) {
            this.assign = assign;
            this.scores = scores;
            this.mean_score = mean_score;
        }
        public ArrayList<Path> getAssign() {
            return assign;
        }
        public ArrayList<Double> getScores() {
            return scores;
        }
        public double getMeanScore() {
            return mean_score;
        }
    }
    
    public Alignment() {
        this.assignments = new ArrayList<>();
        this.scores = new ArrayList<>();
    }
    public void addPair(Path p1, Path p2, double score, boolean blocked) {
        boolean exists = false;
        int cnt = 0;
        for (ArrayList ass_list : assignments) {
            if (ass_list.contains(p1)) {
                exists = true;
                ass_list.add(p2);
                ArrayList loc_scores = scores.get(cnt);
                loc_scores.add(score);
                scores.set(cnt, loc_scores);
                break;
            }
            cnt++;
        }
        if (!exists && !blocked) {
            ArrayList new_list = new ArrayList<>();
            new_list.add(p1);
            new_list.add(p2);
            assignments.add(new_list);
            
            ArrayList new_scrs = new ArrayList<>();
            new_scrs.add(score);
            scores.add(new_scrs);
        }
    }
    public ArrayList<ArrayList<Path>> getAssignments() {
        return assignments;
    }
    public ArrayList<ArrayList<Double>> getScores() {
        return scores;
    }
    public ArrayList<Double> getMeanScores() {
        ArrayList<Double> to_ret = new ArrayList<>();
        for (ArrayList<Double> vals : this.getScores()) {
            StatsMaker sm = new StatsMaker();
            for (double val : vals) {
                sm.afegeixValor(val);
            }
            sm.calculaStats();
            double mean = sm.getMitjana();
            to_ret.add(mean);
        }
        
        sortByScores(to_ret);
        
        return to_ret;
    }

    private void sortByScores(ArrayList<Double> mean_scores) {
        ArrayList<Alignment_Score> to_sort = new ArrayList<>();
        for (int i = 0; i < mean_scores.size(); i++) {
            Alignment_Score as = new Alignment_Score(assignments.get(i),scores.get(i),mean_scores.get(i));
            to_sort.add(as);
        }
        to_sort.sort(new Compare_Align_Score());
        int cnt = 0;
        for (Alignment_Score as : to_sort) {
            assignments.set(cnt, as.getAssign());
            scores.set(cnt, as.getScores());
            mean_scores.set(cnt, as.getMeanScore());
            
            cnt++;
        }
    }
}

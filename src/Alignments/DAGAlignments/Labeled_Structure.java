/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.DAGAlignments;

import Auxiliar.Clustering.Path_Label;
import Forest.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author ASUS
 */
public class Labeled_Structure {
    private HashMap<Path_Label,ArrayList<Path>> grouped_paths;

    public Labeled_Structure() {
        grouped_paths = new HashMap<>();
    }
    public void addNewPath(Path_Label label, Path path) {
        ArrayList<Path> nested = new ArrayList<>();
        if (grouped_paths.containsKey(label)) {
            nested = grouped_paths.get(label);
        }
        nested.add(path);
        grouped_paths.put(label, nested);
    }
    public ArrayList<Path> getPathsForLabel(Path_Label label) {
        if (!grouped_paths.containsKey(label)) {
            return new ArrayList<>();
        } else {
            return grouped_paths.get(label);
        }
    }
    public Set<Path_Label> getLabelSet() {
        return grouped_paths.keySet();
    }
}

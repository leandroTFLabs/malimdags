/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.DAGAlignments;

import Alignments.PathAlignments.PairWise.Path_Aligner;
import Auxiliar.Comparison.MBB_Simil_Data;
import Forest.Path;
import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class Hungarian_Path_Combiner extends Thread {
    /*
    ATRIBUTS
    */
    private Hungarian_Algorithm munkres;
    private MBB_Simil_Data mbb_simils;
    private ArrayList<Path> pl1, pl2;
    private float[][] cost_matrix;
    private int[][] optimal_assignment;
    private ArrayList<Double> assign_scores;
    ///////////////////////////////////////////////
    /*
    CONSTRUCTORS
    */
    public Hungarian_Path_Combiner(ArrayList<Path> pl1, ArrayList<Path> pl2, MBB_Simil_Data mbb_simils) {
        this.pl1 = pl1;
        this.pl2 = pl2;
        this.mbb_simils = mbb_simils;
    }

    @Override
    public void run() {
        initCostMatrix();
        float[][] copy_of_matrix = new float[cost_matrix.length][cost_matrix[0].length];
        for (int i = 0; i < copy_of_matrix.length; i++) {
            System.arraycopy(cost_matrix[i], 0, copy_of_matrix[i], 0, copy_of_matrix[0].length);
        }
        this.munkres = new Hungarian_Algorithm(copy_of_matrix);
        this.optimal_assignment = munkres.findOptimalAssignment();
        this.assign_scores = new ArrayList();
    }
    ///////////////////////////////////////////////
    /*
    GETTERS
    */
    public ArrayList<Path[]> getAssignedPairs() {
        ArrayList<Path[]> to_ret = new ArrayList();
        for (int x = 0; x < optimal_assignment.length; x++) {
            int i = optimal_assignment[x][0];
            int j = optimal_assignment[x][1];
            Path iPath = getIPath(i);
            Path jPath = getJPath(j);
            double score = 1.0 - cost_matrix[i][j];
            if (!iPath.isNoPath() && !jPath.isNoPath()) {
                Path[] pair = new Path[2];
                pair[0] = iPath;
                pair[1] = jPath;
                to_ret.add(pair);
                assign_scores.add(score);
            }
        }
        return to_ret;
    }
    public ArrayList<Double> getScores() {
        return assign_scores;
    }
    ///////////////////////////////////////////////
    /*
    SETTERS
    */
    ///////////////////////////////////////////////
    /*
    ALTRES MÈTODES AUXILIARS
    */
    private Path getIPath(int i) {
        Path to_ret = i >= pl1.size() ? Path.noPath() : pl1.get(i);
        return to_ret;
    }
    private Path getJPath(int j) {
        Path to_ret = j >= pl2.size() ? Path.noPath() : pl2.get(j);
        return to_ret;
    }
    private void initCostMatrix() {
        int tam = Math.max(pl1.size(),pl2.size());
        cost_matrix = new float[tam][tam];
        for (int i = 0; i < tam; i++) {
            for (int j = 0; j < tam; j++) {
                boolean exists = i < pl1.size() && j < pl2.size();
                if (exists) {
                    Path_Aligner pwa = new Path_Aligner(pl1.get(i),pl2.get(j),mbb_simils);
                    cost_matrix[i][j] = (float) (1.0 - pwa.getBestCompatibility());
                } else cost_matrix[i][j] = Float.MAX_VALUE;
            }
        }
    }
    ///////////////////////////////////////////////
}

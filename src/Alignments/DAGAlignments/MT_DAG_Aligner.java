/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.DAGAlignments;

import Auxiliar.Clustering.Path_Label;
import Auxiliar.Comparison.MBB_Simil_Data;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author ASUS
 */
public class MT_DAG_Aligner {
    /*
    ATRIBUTS
    */
    private ArrayList<String> dag_names;
    private ArrayList<Labeled_Structure> dags;
    private MBB_Simil_Data mbb_simils;

    private Labeled_Alignment final_alignment;

    private int max_sample_size;
    ///////////////////////////////////////////////
    /*
    CONSTRUCTORS
    */
    public MT_DAG_Aligner(ArrayList<String> dag_names, ArrayList<Labeled_Structure> dags, int max_sample_size) throws IOException {
        this.dag_names = dag_names;
        this.dags = dags;
        this.mbb_simils = new MBB_Simil_Data();
        this.final_alignment = null;
        this.max_sample_size = max_sample_size;
        processMTAlignment();
    }
    ///////////////////////////////////////////////
    /*
    GETTERS
    */
    public Labeled_Alignment getFinalAlignment() {
        return final_alignment;
    }
    ///////////////////////////////////////////////
    /*
    SETTERS
    */
    ///////////////////////////////////////////////
    /*
    ALTRES MÈTODES AUXILIARS
    */
    private void processMTAlignment() {
        HashMap<Path_Label,ArrayList<String>> sorted_dags = new HashMap<>();
        for (int i = 1; i < this.dags.size(); i++) {
            // Labeled_Structure lst = dags.get(i-1);
            // Labeled_Structure rst = dags.get(i);
            PW_DAG_Aligner partial_align = new PW_DAG_Aligner(dag_names,dags,final_alignment,max_sample_size,mbb_simils,sorted_dags);
            sorted_dags = partial_align.getSortedByNow();

            int desired_sample = 1;
            int cnt = 0;
            for (Path_Label pl : sorted_dags.keySet()) {
                if (cnt < desired_sample) System.out.println(pl+" --> "+sorted_dags.get(pl));
                cnt++;
            }

            final_alignment = partial_align.getAlignment();
            final_alignment.blockAlignment();
        }
        final_alignment.setAlignmentOrder(sorted_dags);
    }
    ///////////////////////////////////////////////
}

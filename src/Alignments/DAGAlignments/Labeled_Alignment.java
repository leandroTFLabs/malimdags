/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.DAGAlignments;

import Auxiliar.Clustering.Path_Label;
import Auxiliar.Comparison.Compare_Labels;
import Disk_Mgmt.Class_Labeler;
import Forest.Path;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author ASUS
 */
public class Labeled_Alignment {
    
    private HashMap<Path_Label,Alignment> labeled_groups;
    private boolean blocked = false;

    private HashMap<Path_Label,ArrayList<String>> alignment_order;

    public Labeled_Alignment() {
        this.labeled_groups = new HashMap<>();
    }

    public void addLabeledPair(Path_Label label, Path p1, Path p2, double score) {
        Alignment alig = new Alignment();
        if (labeled_groups.containsKey(label)) {
            alig = labeled_groups.get(label);
        }
        alig.addPair(p1, p2, score, blocked);
        labeled_groups.put(label, alig);
    }
    public void setAlignmentOrder(HashMap<Path_Label,ArrayList<String>> alignment_order) {
        this.alignment_order = alignment_order;
    }

    public Alignment getLabeledAlignment(Path_Label label) {
        if (!labeled_groups.containsKey(label)) {
            return new Alignment();
        } else {
            return labeled_groups.get(label);
        }
    }
    private ArrayList<Path_Label> grabOrderedLabels() {
        ArrayList<Path_Label> orderedLabels = new ArrayList<>();
        for (Path_Label pl : labeled_groups.keySet()) {
            orderedLabels.add(pl);
        }
        orderedLabels.sort(new Compare_Labels());
        return orderedLabels;
    }
    public void blockAlignment() {
        blocked = true;
    }
    public void dumpIntoFile(ArrayList<String> dag_names, boolean bear_sigm) throws IOException {
        String currentDir = System.getProperty("user.dir");
        String maFName = currentDir.concat("/DATA/calculated/MA-ready-alignments/MA-ready-alignments.txt");
        File maF = new File(maFName);
        FileWriter fw = new FileWriter(maF);
        BufferedWriter bw = new BufferedWriter(fw);
        Class_Labeler labeler = new Class_Labeler(bear_sigm,null);
        
        String names_line = "";
        for (String dag_name : dag_names) {
            names_line += dag_name + "\t";
        }
        names_line += "\r\n";
        
        bw.write(names_line);
        
        for (Path_Label key : this.grabOrderedLabels()) {
            if (labeled_groups.get(key).getAssignments().size() > 0) {
                String label_descriptor = labeler.getLabelDescription(key.toString());
                bw.write("//////////////////////////////////////////////////////////////\r\n");
                String label_line = key.conditionedToString() + "\t=\t" + label_descriptor + "\r\n";
                bw.write(label_line);
                bw.write("--------------------------------------------------------------\r\n");
                bw.write(this.alignment_order.get(key).toString());
                bw.write("\r\n");
                bw.write("--------------------------------------------------------------\r\n");

                int cnt = 0;
                for (ArrayList<Path> assign : labeled_groups.get(key).getAssignments()) {
                    double score = labeled_groups.get(key).getMeanScores().get(cnt);

                    String assign_line = "";
                    for (Path p : assign) {
                        assign_line += p.getDescriptor() + "\t";
                    }
                    assign_line += "\t\t\tscore=" + score + "\r\n";

                    bw.write(assign_line);

                    cnt++;
                }
            }
        }
        
        bw.close();
        fw.close();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QueryTool.querying;

import Auxiliar.Comparison.MBB_Simil_Data;
import Disk_Mgmt.Class_Labeler;
import Disk_Mgmt.MBB_Labeler;
import Disk_Mgmt.MBB_Reactions;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class Query_Data {
    
    private MBB_Labeler mbb_labels;
    private Class_Labeler set_labels_s;
    private Class_Labeler set_labels_ns;
    private MBB_Reactions mbb_reactions;
    private MBB_Simil_Data mbb_simils;
    
    public Query_Data() {
        try {
            this.mbb_labels = new MBB_Labeler(this);
            this.mbb_reactions = new MBB_Reactions(this);
            this.set_labels_ns = new Class_Labeler(false,this);
            this.set_labels_s = new Class_Labeler(true,this);
            this.mbb_simils = new MBB_Simil_Data();
        } catch (IOException ex) {
            Logger.getLogger(Query_Data.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getMBBId(String mbb_label) {
        String transf = mbb_label.startsWith("MBBL") ? mbb_label : "MBBL".concat(mbb_label);
        return mbb_labels.getMBBBioId(transf);
    }
    
    public Set_Queried getSetInfo(String set_descriptor) {
        String transf = set_descriptor.startsWith("set-") ? set_descriptor : "set-".concat(set_descriptor);
        Set_Queried sq;
        if (transf.contains("_")) {
            sq = new Set_Queried(set_labels_s.getLabelDescription(transf));
        } else {
            sq = new Set_Queried(set_labels_ns.getLabelDescription(transf));
        }
        return sq;
    }
    
    public ArrayList<String> getMBBReactions(String mbb_id) {
        return mbb_reactions.findMBBReactions(mbb_id);
    }
    
    public double getMBBSimils(String mbb_1, String mbb_2) {
        return this.mbb_simils.getSimilMBBs(mbb_1, mbb_2);
    }
    
    public String reactionLink(String reaction_code) {
        return "https://www.genome.jp/dbget-bin/www_bget?rn:"+reaction_code;
    }
    
    public String pathwayLink(String pathway_code) {
        return "https://www.genome.jp/kegg-bin/show_pathway?map"+pathway_code;
    }
}

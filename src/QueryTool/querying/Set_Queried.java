/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QueryTool.querying;

import Auxiliar.Utils;
import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class Set_Queried {
    private ArrayList<String> pathways;
    private String sigm_descript;
    public Set_Queried(String pathways_descript) {
        String transf = pathways_descript.replace("\t","").replace(" ","");
        if (transf.contains("-")) {
            String[] parts = transf.split("-");
            this.pathways = Utils.listFromString(parts[0]);
            this.sigm_descript = parts[1];
        } else {
            this.pathways = Utils.listFromString(transf);
            this.sigm_descript = "Any";
        }
    }
    public ArrayList<String> getPathways() {
        return pathways;
    }
    public String getSigmDescriptor() {
        return sigm_descript;
    }
}

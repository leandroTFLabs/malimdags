/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QueryTool.querying;

import java.io.IOException;

/**
 *
 * @author ASUS
 */
public class Open_Links {
    public static void open_reaction(String reaction_code) throws IOException {
        Runtime rt = Runtime.getRuntime();
        String url = "https://www.genome.jp/dbget-bin/www_bget?rn:"+reaction_code;
        rt.exec("rundll32 url.dll,FileProtocolHandler " + url);
    }
    public static void open_pathway(String pathway_code) throws IOException {
        Runtime rt = Runtime.getRuntime();
        String url = "https://www.genome.jp/kegg-bin/show_pathway?map"+pathway_code;
        rt.exec("rundll32 url.dll,FileProtocolHandler " + url);
    }
}

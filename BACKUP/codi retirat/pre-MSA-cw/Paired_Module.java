/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.PathAlignments;

import java.util.Arrays;

/**
 *
 * @author ASUS
 */
public class Paired_Module {
    private Object[] seqs;
    private double[][] similMatrix;
    public Paired_Module(Object[] seqs, double[][] similMatrix) {
        this.seqs = seqs;
        this.similMatrix = similMatrix;
    }
    public Object[] getSeqs() {
        return seqs;
    }
    public double[][] getSimilMatrix() {
        return similMatrix;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Arrays.deepHashCode(this.seqs);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Paired_Module other = (Paired_Module) obj;
        if (!Arrays.deepEquals(this.seqs, other.seqs)) {
            return false;
        }
        return true;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.PathAlignments;

import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author ASUS
 */
public class MT_Greedy_Sequence_Aligner {
    
    private ArrayList<Paired_Module> pairs_matrixes_list;
    private ArrayList<Pair_Aligned> aligned_pairs;
    private HashSet<Pair_Aligned> pairs_aligns_map;
    private HashSet<Paired_Module> already_placed;
    
    public MT_Greedy_Sequence_Aligner(ArrayList included_elements, ArrayList<Paired_Module> similMatrixes) {
        this.pairs_matrixes_list = similMatrixes;
        this.pairs_aligns_map = new HashSet();
        this.already_placed = new HashSet();
        
        proceedToAlign();
    }
    
    private void proceedToAlign() {
        boolean first_round = true;
        while (!pairs_matrixes_list.isEmpty()) {
            processAllPairs(first_round);
            sortPairs();
            if (first_round) first_round = false;
            removeFirstPair();
        }
    }
    
    private void processAllPairs(boolean first_round) {
        aligned_pairs = new ArrayList();
        for (Paired_Module pm : pairs_matrixes_list) {
            PW_Dynamic_Sequence_Aligner pwalign = new PW_Dynamic_Sequence_Aligner(pm.getSimilMatrix());
            Pair_Aligned pa = new Pair_Aligned(pm,pwalign);
            if (first_round) pairs_aligns_map.add(pa);
            aligned_pairs.add(pa);
        }
    }
    
    private void sortPairs() {
        aligned_pairs.sort(new Compare_Aligned_Pairs());
    }

    private void removeFirstPair() {
        Paired_Module pair = aligned_pairs.get(0).getPair();
        already_placed.add(pair);
        pairs_matrixes_list.remove(pair);
    }
}

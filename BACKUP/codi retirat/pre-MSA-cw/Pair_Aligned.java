/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignments.PathAlignments;

import java.util.Objects;

/**
 *
 * @author ASUS
 */
public class Pair_Aligned {
    private Paired_Module pair;
    private PW_Dynamic_Sequence_Aligner alignment;
    public Pair_Aligned(Paired_Module pair, PW_Dynamic_Sequence_Aligner alignment) {
        this.pair = pair;
        this.alignment = alignment;
    }
    public Paired_Module getPair() {
        return pair;
    }
    public PW_Dynamic_Sequence_Aligner getAlignment() {
        return alignment;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.pair);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pair_Aligned other = (Pair_Aligned) obj;
        if (!Objects.equals(this.pair, other.pair)) {
            return false;
        }
        return true;
    }
    
}
